# Hello, NoCommentian, this is our repository GIT
## Summary
1. Installation
2. Generating your keys
3. Cloning Repository
4. Commiting and pushing

### Installation
to contribute to this project you must have git isntalled in your machine.
Link to download GIT : https://git-scm.com/
it's also recommended to install Gitkraken, a GUI client for git.
Link : https://www.gitkraken.com/

### Generating keys
To push your changes you may need to generate keys.
this is a tutorial to generate keys : https://docs.gitlab.com/ce/ssh/README.html

### Cloning Repository
to clone this repository, you can download it by clicking on download icon or by taping this command on your command-line interface
`git clone git@gitlab.com:No-Comment/artsuki-javaEE.git`
`git clone https://gitlab.com/No-Comment/artsuki-javaEE.git` 
and place it to your repository.

### Commiting and pushing
for those who wants to use command-line interface as git client this usefull command to commit and push
`To add your edited file to your commit`
> git add .
`To Commit your changes`
> git commit -m "your message here"
`To push your changes`
> git push origin master

PS: DO NOT FORGET TO PULL LAST VERSION OF THE PROJECT IN YOUR MACHINE BEFORE ADDING ANY CHANGES
> git pull remote master -u
for More information, you can visit this gitlab page
https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html

### Conclusion
Idea and direction By : SAMI EL FEHRI
2017 / 2018