package eventmodule;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import ejb.ProjectNotFoundException;
import ejb.RatingRemote;
import ejb.artsuki.ArtsukiRemote;
import ejb.event.EventLocal;
import entity.Artsuki;
import entity.Event;
import entity.EventCategory;
import entity.Project;
import entity.Role;
import projectmodule.AuthenticatedUser;
import projectmodule.Secured;
import entity.Rating;



@Path("event")
@ManagedBean
public class EventResource {
	@EJB
	private EventLocal eventManager;
	@EJB
	private ArtsukiRemote artsukiManager;	
	@EJB
	private RatingRemote ratingManager;
	@Inject
	@AuthenticatedUser
	Artsuki authenticatedUser;
	


	@POST
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postEvent(Event event) {
	//	System.out.println("auuur" + authenticatedUser.getId());
		Artsuki a=artsukiManager.findArtsukiById(authenticatedUser.getId());
		event.setPublisher(a);
		if( eventManager.addEvent(event) ) {
			return Response.status(Status.CREATED).entity("Event Created").build();
		}
		return Response.status(Status.NOT_ACCEPTABLE).entity("Failed to create this Event").build();
		}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("artsuki")
	public Response postArtsukiRate() {
		if( eventManager.AddRateArtsuki() ) {
			return Response.status(Status.CREATED).entity("artsuki Created").build();
		}
		return Response.status(Status.NOT_ACCEPTABLE).entity("Failed to create this artsuki").build();
		}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEvents() {
		return Response.ok(eventManager.displayEventAvailable()).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("archived")
	public Response getEventsArchived() {
		return Response.ok(eventManager.displayEventArchived()).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("all")
	public Response getEventsAll() {
	     List<Event> evts=eventManager.displayEventAvailable();

		for(Event e:evts) {	
        	eventManager.Archive(e);			
				}
		return Response.ok(eventManager.displayEvent()).build();
	}
	
	@PUT
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response putEvent(Event event)  {
		Artsuki a=artsukiManager.findArtsukiById(authenticatedUser.getId());
		Event e = eventManager.getIdEvent(event.getId());

		if(a.getId()==e.getPublisher().getId()) {
		if(eventManager.updateEvent(event)) {
			return Response.status(Status.CREATED).entity("Event updated").build();
		}else return Response.status(Status.NOT_MODIFIED).entity("Failed to update event").build();
		
	}else return Response.status(Status.NOT_MODIFIED).entity("Only event's publisher can update it").build();

		}
		
	@DELETE
	@Secured
	@Path("{event}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteProject(@PathParam(value="event") int event) {
		Artsuki a=artsukiManager.findArtsukiById(authenticatedUser.getId());
		Event e = eventManager.getIdEvent(event);
		if(a.getRole()==Role.ADMIN || (e.getPublisher().getId()==a.getId() && e.getParticipant().size()==0)) {
		if(eventManager.removeEvent(event) ) {
			return Response.status(Status.OK).entity("Event deleted").build();
			} }System.out.println("");
				return Response.status(Status.METHOD_NOT_ALLOWED).entity("you can delete this event if you're an Admin or the event has no participants").build();
			}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("fin/{fin}")
	public Response getEventHekka2(@PathParam(value="fin") int id) {
		
			return Response.ok(eventManager.getIdEvent(id)).build();
		
	}	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("show")
	public Response getEventHekka(Event event) {
		
			return Response.ok(eventManager.getIdEvent(event.getId())).build();
		
	}	
	@PUT
	@Path("/part")
	@Secured
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addParticipant(@FormParam(value="eventID") int eventID) {
		Artsuki a=artsukiManager.findArtsukiById(authenticatedUser.getId());
	Event e = eventManager.getIdEvent(eventID);
	//System.out.println("d"+eventManager.ParticipantExist(e, a));
	if(eventManager.ParticipantExist(e, a)!=2) {
	if(e.getPublisher().getId()!=a.getId()) {
		if( eventManager.addParticipants(eventID,a.getId()) && (a.getRole()==Role.ADMIN ||a.getRole()==Role.ARTIST )) {
			
			return Response.status(Status.OK).entity("participant added").build();
		}		return Response.status(Status.BAD_REQUEST).entity("failed to add participant").build();
}else	return Response.status(Status.FORBIDDEN).entity("you can not participate to your own event").build();
	}else return Response.status(Status.NOT_ACCEPTABLE).entity("you already participated").build();

	}
	@PUT
	@Path("/archive")
	@Produces(MediaType.APPLICATION_JSON)
	public Response Archive() {
	     List<Event> evts=eventManager.displayEventAvailable();
			

	        for(Event e:evts) {	
	        	eventManager.Archive(e);			
					}return Response.status(Status.OK).entity("archived").build();				
	}
	
	@GET
	@Path("{category}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response FindEventWithCategory(@PathParam(value="category") EventCategory category) {
		System.out.println("Category : "+category);
			return Response.status(Status.OK).entity(eventManager.findEventByCategory(category)).build();
	}
	@GET
	@Path("findBy")
	@Produces(MediaType.APPLICATION_JSON)
	public Response FindEventWithdCategory(@Context UriInfo variable) {
		String date=variable.getQueryParameters().getFirst("date");
		String place=variable.getQueryParameters().getFirst("place");
        String category=variable.getQueryParameters().getFirst("category");

		if(place != null)
		{
			return Response.status(Status.OK).entity(eventManager.findEventByPlace(place)).build();
		}
		if( EventCategory.valueOf(category)!=null )
		{
			return Response.status(Status.OK).entity(eventManager.findEventByCategory(EventCategory.valueOf(category))).build();
		}
		if( Timestamp.valueOf(date)!=null)
		{
			return Response.status(Status.OK).entity(eventManager.findEventByDate(Timestamp.valueOf(date))).build();
			
		}else return Response.status(Status.NO_CONTENT).entity("did not find any event").build();
 
	}
    
	@POST
	@Path("list")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEventsParticipant(Event event) {
		
			return Response.status(Status.FOUND).entity(eventManager.displayParticipant(event)).build();
		
	}
	@GET
	@Path("order")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getListOrderByDate() {
		
			return Response.status(Status.FOUND).entity(eventManager.displayOrder()).build();
		
	}
	
	@POST
	@Path("avg")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRating(Event event) {
		
			return Response.status(Status.FOUND).entity(eventManager.AvgRate(event)).build();
		
	}

	
	
	}
	
	

