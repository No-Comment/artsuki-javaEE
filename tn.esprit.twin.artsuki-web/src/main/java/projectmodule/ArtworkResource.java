package projectmodule;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ejb.ArtworkNotFoundException;
import ejb.ArtworkRemote;
import ejb.artsuki.ArtsukiRemote;
import entity.Artsuki;
import entity.Artwork;
import entity.CategoryArtwork;

@Path("artwork")
@RequestScoped
public class ArtworkResource {
	@EJB
	private ArtworkRemote artworkManager;

	@EJB
	private ArtsukiRemote artsukiManager;

	@Inject
	@AuthenticatedUser
	Artsuki authenticatedUser;

	@POST
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addArtworkService(Artwork artwork) {
		Artsuki artsuki = artsukiManager.findArtsukiById(authenticatedUser.getId());
		artwork.setPublisher(artsuki);
		if (artworkManager.addArtwork(artwork)) {
			return Response.status(Status.CREATED).entity(artwork).build();
		} else
			return Response.status(Response.Status.BAD_REQUEST).entity("{\"Message\":\"Missing attributes!\"}").build();
	}

	@PUT
	@Path("/put")
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modifyArtwork(Artwork artwork) {
		Artsuki artsuki = artsukiManager.findArtsukiById(authenticatedUser.getId());
		try {
			if (artwork != null) {
				artworkManager.modifyArtwork(artwork, artsuki.getId());
				return Response.status(Status.ACCEPTED).entity(artwork).build();
			} else
				return Response.notModified("{\"Message\":\"You are not allowed to update this Artwork!\"}").build();
		} catch (ArtworkNotFoundException e) {
			return Response.status(Status.NOT_MODIFIED).entity(e.getMessage()).build();
		}
	}
	
	@GET
	@Path("/sell")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sellArtwork(@QueryParam("id") int id) {
		try {
			if (id != 0) {
				artworkManager.sellArtwork(id);
				return Response.status(Status.ACCEPTED).build();
			} else
				return Response.notModified("{\"Message\":\"You are not allowed to update this Artwork!\"}").build();
		} catch (ArtworkNotFoundException e) {
			return Response.status(Status.NOT_MODIFIED).entity(e.getMessage()).build();
		}
	}

	@DELETE
	@Path("/delete")
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteArtwork(Artwork artwork) {
		Artsuki artsuki = artsukiManager.findArtsukiById(authenticatedUser.getId());
		try {
			if (artwork != null) {
				artworkManager.deleteArtwork(artwork, artsuki.getId());
				return Response.ok("{\"Message\":\"Your Artwork has been deleted successfully!\"}").build();
			} else
				return Response.notModified("{\"Message\":\"You are not allowed to delete this Artwork!\"}").build();
		} catch (ArtworkNotFoundException e) {
			return Response.status(Status.NOT_ACCEPTABLE).entity(e.getMessage()).build();
		}
	}
	
	@DELETE
	@Path("/supprimer")
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteArtwork(@QueryParam("id") int id ) {
		Artsuki artsuki = artsukiManager.findArtsukiById(authenticatedUser.getId());
		try {
			if (id != 0) {
				artworkManager.deleteArtworkById(id, artsuki.getId());
				return Response.ok("{\"Message\":\"Your Artwork has been deleted successfully!\"}").build();
			} else
				return Response.notModified("{\"Message\":\"You are not allowed to delete this Artwork!\"}").build();
		} catch (ArtworkNotFoundException e) {
			return Response.status(Status.NOT_ACCEPTABLE).entity(e.getMessage()).build();
		}
	}

	@GET
	@Path("/main")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllArtworks() {
		List<Artwork> artworks = artworkManager.getAllArtwork();
		if (artworks.size() != 0) {
			return Response.status(Status.ACCEPTED).entity(artworks).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("{\"Message\":\"Empty List :/ \"}").build();
	}
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllArtworks(@QueryParam("id") int id) {
		Artwork artwork = artworkManager.findById(id);
		if (artwork != null) {
			return Response.status(Status.ACCEPTED).entity(artwork).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("{\"Message\":\"Empty List :/ \"}").build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getArtworkByCategory(@QueryParam("category") String category) {
		List<Artwork> artworks = artworkManager.findByCategory(category);
		if (artworks.size() != 0) {
			return Response.status(Status.ACCEPTED).entity(artworks).build();
		} else
			return Response.status(Response.Status.ACCEPTED).entity("{\"Message\":\"Empty List :/ \"}").build();
	}

	@GET
	@Path("/publisher")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getArtworkByCategory(@QueryParam("idPublisher") int idPublisher) {
		List<Artwork> artworks = artworkManager.findByPublisher(idPublisher);
		if (artworks.size() != 0) {
			return Response.status(Status.ACCEPTED).entity(artworks).build();
		} else
			return Response.status(Response.Status.ACCEPTED).entity("{\"Message\":\"Empty List :/ \"}").build();
	}

	@GET
	@Path("/filter")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response filterArtworksByPrice(@QueryParam("minPrice") double minPrice,
			@QueryParam("maxPrice") double maxPrice) {
		List<Artwork> artworks = new ArrayList<Artwork>();
		artworks = artworkManager.filterByPrice(minPrice, maxPrice);
		if (artworks.size() != 0) {
			return Response.status(Status.ACCEPTED).entity(artworks).build();
		} else
			return Response.status(Response.Status.ACCEPTED).entity("{\"Message\":\"Empty List :/ \"}").build();
	}

	@GET
	@Path("/criteria")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response criteriaSearch(
			@QueryParam("category") CategoryArtwork category, 
			@QueryParam("price") Double price,
			@QueryParam("media") String mediaType) {
		List<Artwork> artworks = new ArrayList<Artwork>();
		artworks = artworkManager.findByCriteria(category, price, mediaType);
		if (artworks.size() != 0) {
			return Response.status(Status.ACCEPTED).entity(artworks).build();
		} else
			return Response.status(Response.Status.ACCEPTED).entity("{\"Message\":\"Empty List :/ \"}").build();
	}
}
