package projectmodule;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ejb.CommentRemote;
import ejb.CommunityRemote;
import ejb.artsuki.ArtsukiRemote;
import entity.Artsuki;
import entity.Comment;
import entity.Community;

@Path("comment")
@ManagedBean
public class CommentAPI{
	@EJB
	private CommentRemote commentManager;
	@EJB
	private ArtsukiRemote artsukiManager;	
	static List<Comment> listComment= new ArrayList<Comment>();
	@Inject
	@AuthenticatedUser
	Artsuki authenticatedUser;
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCommentService(Comment comment) {
		Artsuki a=artsukiManager.findArtsukiById(authenticatedUser.getId());
		comment.setCommenter(a);
		commentManager.addComment(comment);
			return Response.status(Status.CREATED).entity("done").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getComments() {
		Comment comment = commentManager.findById(81);
		return Response.ok(comment).build();
	}
	
	@GET
	@Path("theSignled")
	@Produces(MediaType.APPLICATION_JSON)
	public Response theMostRatef() {
		Comment comment = commentManager.findThatComment();
		return Response.ok(Status.FOUND).entity(comment).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modifyComment(Comment comment, @QueryParam("id") int id) {
		commentManager.modifyComment(comment, id);
			return Response.ok("Your Comment has been modified!").build();
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteComment(Comment comment){
		if (commentManager.deleteById(comment.getId())) {
			return Response.ok("Your Comment has been deleted successfully!").build();
		}
		return Response.status(Status.NOT_ACCEPTABLE).entity("Comment not found or server error!").build();
	}
}