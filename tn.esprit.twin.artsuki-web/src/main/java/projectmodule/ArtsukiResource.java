package projectmodule;


import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ejb.artsuki.ArtsukiRemote;
import ejb.artsuki.MailNotFoundException;
import entity.Artsuki;
import entity.Role;

@Path("artsuki")
@ManagedBean
public class ArtsukiResource {

	@EJB
	private ArtsukiRemote artsukiManager;

	@Inject
	@AuthenticatedUser
	Artsuki authenticatedUser;
	
	@GET
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOnlineArtsuki() {
			Artsuki artsukiDB = artsukiManager.findArtsukiById(authenticatedUser.getId());
			return Response.status(Status.OK).entity(artsukiDB).build();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getArtsukiDetails(Artsuki artsuki)
	{
		if(artsuki.getId() != 0)
		{
			Artsuki artsukiDB = artsukiManager.findArtsukiById(artsuki.getId());
			Artsuki artsukiToSend=new Artsuki();
			artsukiToSend.setFirstname(artsukiDB.getFirstname());
			artsukiToSend.setLastname(artsukiDB.getLastname());
			artsukiToSend.setGender(artsukiDB.getGender());
			artsukiToSend.setRole(artsukiDB.getRole());
			artsukiToSend.setArt(artsukiDB.getArt());
			artsukiToSend.setEmail(artsukiDB.getEmail());
			artsukiToSend.setBiography(artsukiDB.getBiography());
			return Response.status(Status.OK).entity(artsukiToSend).build();
		}
		return Response.status(Status.NO_CONTENT).build();
		
	}
	

	@POST
	@Path("register")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerArtsuki(Artsuki artsuki) {
		String registrationErrors = artsukiManager.isValidRegistration(artsuki);
		if (registrationErrors == "") {
			artsukiManager.registerArtsuki(artsuki);
		} else
			return Response.status(Response.Status.BAD_REQUEST).entity(registrationErrors).build();
		return Response.status(Response.Status.OK).entity("Artsuki subscribed").build();
	}
	
	@GET
	@Path("register/confirm")
    public Response confirmRegistrationArtsuki(@QueryParam("token") String token)
    {
		try{
		artsukiManager.confirmAccount(token,"confirmation888x");
		return Response.ok().build();
		}
		catch(Exception nae)
		{
		return Response.status(Status.BAD_REQUEST).build();
		}
		
    }
	@PUT
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateArtsuki(Artsuki artsuki) {
		
		if(artsuki.getId() == authenticatedUser.getId() || authenticatedUser.getRole() == Role.ADMIN)
		{
		artsukiManager.updateArtsuki(artsuki);
		return Response.status(Response.Status.OK).entity("Artsuki successfuly updated").build();
		}
		return Response.status(Status.METHOD_NOT_ALLOWED).build();
	}
	
	@PUT
	@Secured
	@Path("upgrade")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response upgradeArtsuki(Artsuki artsuki)
	{
		if(authenticatedUser.getRole() == Role.ADMIN)
		{
		String requestStatus=artsukiManager.upgradeToArtist(artsuki);
		if(requestStatus.equals("Succesfully upgraded"))
			return Response.status(Status.OK).entity(requestStatus).build();
		
		
		return Response.status(Status.BAD_REQUEST).entity(requestStatus).build();
		}
		return Response.status(Status.METHOD_NOT_ALLOWED).build();
	}

	
	@GET
	@Path("search")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchArtsuki(@QueryParam("data") String data) {
		return Response.status(Status.OK).entity((artsukiManager.findArtsuki(data).size()==0)?"{\"message\":\"Nothing to show\"}":artsukiManager.findArtsuki(data)).build();
	}
	

	@DELETE
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteArtsuki(Artsuki artsuki) {
		if(authenticatedUser.getRole()==Role.ADMIN)
			{artsukiManager.deleteArtsuki(artsuki);
			return Response.status(Response.Status.OK).entity("Artsuki successfuly deleted").build();
			}
			
		return Response.status(Response.Status.NOT_ACCEPTABLE).build();
	}

	
	@GET
	@Path("recovery")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response recoverPasswordRequest(@QueryParam("mail") String mail)
	{
		try {
			artsukiManager.sendRecoveryMail(mail);
		} catch (MailNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}
	
	@POST
	@Path("recovery")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response recoverPassword(@QueryParam("token") String token,Artsuki artsuki)
	{
		try{
			artsukiManager.updatePassword(token, artsuki);
			return Response.ok().build();
		}
		catch(Exception e)
		{
			return Response.status(Status.BAD_REQUEST).build();
		}
	}
}
