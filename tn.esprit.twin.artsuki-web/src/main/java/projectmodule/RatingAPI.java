package projectmodule;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ejb.CommunityRemote;
import ejb.RatingRemote;
import entity.Comment;
import entity.Community;
import entity.Rating;

@Path("rating")
@ManagedBean
public class RatingAPI {
	@EJB
	private RatingRemote ratingManager;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addRatingService(Rating rating) {
		ratingManager.addRate(rating);
			return Response.status(Status.CREATED).entity("done").build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRates() {
		Rating rating = ratingManager.findRating(47);
		return Response.ok(rating).build();
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRate(Rating rating){
		ratingManager.deleteRate(rating.getId());
		return Response.ok("Your Comment has been deleted successfully!").build();	
	}
	
	@GET
	@Path("theMost")
	@Produces(MediaType.APPLICATION_JSON)
	public Response theMostRatef() {
		Float rating = ratingManager.max();
		return Response.ok(Status.FOUND).entity(rating).build();
	}
	
	@GET
	@Path("lId")
	@Produces(MediaType.APPLICATION_JSON)
	public Response theMost() {
		int rating = ratingManager.idMax();
		return Response.ok(Status.FOUND).entity(rating).build();
	}
}