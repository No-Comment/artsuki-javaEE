package projectmodule;

import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ejb.RatingRemote;
import ejb.artsuki.ArtsukiRemote;
import entity.Artsuki;
import entity.Rating;

@Path("ratings")
@RequestScoped
public class RatingRessource {
	@EJB
	private RatingRemote ratingManager;

	@EJB
	private ArtsukiRemote artsukiManager;
	
	@Inject
	@AuthenticatedUser
	Artsuki authenticatedUser;

	@Secured
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addRatingService(Rating rating) {
		Artsuki artsuki = artsukiManager.findArtsukiById(authenticatedUser.getId());
		rating.setRater(artsuki);
		if (ratingManager.addRatingToArtwork(rating, artsuki.getId())) {
			return Response.status(Status.CREATED).entity("{\"Message\":\"Your Rating is added successfully!\"}")
					.build();
		} else
			return Response.status(Response.Status.NOT_ACCEPTABLE).entity("{\"Message\":\"You have already rated this publication!\"}").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRatesByArtwork(@QueryParam("idArtwork") int idArtwork) {
		float rates = ratingManager.findByArtwork(idArtwork);
		return Response.status(Status.ACCEPTED).entity(rates).build();
	}

}
