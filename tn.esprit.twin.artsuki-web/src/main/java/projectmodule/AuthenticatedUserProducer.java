package projectmodule;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import ejb.artsuki.ArtsukiRemote;
import entity.Artsuki;

@RequestScoped
public class AuthenticatedUserProducer {

	@EJB
	private ArtsukiRemote artsukiManager;

    @Produces
    @RequestScoped
    @AuthenticatedUser
    private Artsuki authenticatedUser;

    public void handleAuthenticationEvent(@Observes @AuthenticatedUser String mail) {
        this.authenticatedUser = findUser(mail);
    }

    private Artsuki findUser(String mail) {
        // Hit the the database or a service to find a user by its username and return it
        // Return the User instance
    	return artsukiManager.findArtsukiByEmail(mail);
    }

}
