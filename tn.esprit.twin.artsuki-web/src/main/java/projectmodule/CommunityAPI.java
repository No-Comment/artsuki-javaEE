package projectmodule;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ejb.CommunityRemote;
import ejb.RatingRemote;
import entity.Artsuki;
import entity.Comment;
import entity.Community;
import entity.Rating;

@Path("community")
@RequestScoped
public class CommunityAPI{
	@EJB
	private CommunityRemote communityManager;
	@EJB
	private RatingRemote ratingManager;
	static List<Community> listCommunity= new ArrayList<Community>();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCommunityService(Community community) {
	communityManager.addCommunity(community);
			return Response.status(Status.CREATED).entity("done").build();
			//return Response.status(Status.NOT_ACCEPTABLE).entity("failed").build();
	}
	
//	@POST
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response addRatingCommunityService(Community community, Rating rating) {
//	int comID = communityManager.addCommunity(community);
//	community.setId(comID);
//	int rateId = communityManager.addRating(rating);
//	rating.setPublication(community);
//	
//			return Response.status(Status.CREATED).entity("done").build();
//			//return Response.status(Status.NOT_ACCEPTABLE).entity("failed").build();
//	}
	
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response getPosts(@QueryParam("id") int id) {
//		Community community = communityManager.findCommunity(id);
//		return Response.ok(Status.FOUND).entity(community).build();
//	}
	
	@GET
	@Path("allCom")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPosts() {
		listCommunity = communityManager.getAllCommunity();
		return Response.ok(Status.FOUND).entity(listCommunity).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLePost(
			@QueryParam("titre") String titre,
			@QueryParam("theme") String theme
			) {
		if (titre!=null) {
			Community community = communityManager.getThePost(titre);
			return Response.ok(Status.FOUND).entity(community).build();
		}
		if (theme!=null) {
			Community community = communityManager.getThePostWithTheme(theme);
		    return Response.ok(Status.FOUND).entity(community).build();
		}
		    return Response.ok(Status.NOT_FOUND).entity("NO POSTS").build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByUser(@QueryParam("name") String name) {
		listCommunity = communityManager.getThePostWithUsername(name);
		return Response.ok(Status.FOUND).entity(listCommunity).build();
	}
	
	@GET
	@Path("theMost")
	@Produces(MediaType.APPLICATION_JSON)
	public Response theMostRatef() {
		Community community = communityManager.theMostLiked();
		return Response.ok(Status.FOUND).entity(community).build();
	}
	
	@GET
	@Path("theLast")
	@Produces(MediaType.APPLICATION_JSON)
	public Response theLastRatef() {
		Community community = communityManager.theLastLiked();
		return Response.ok(Status.FOUND).entity(community).build();
	}
	
	@GET
	@Path("theSignled")
	@Produces(MediaType.APPLICATION_JSON)
	public Response manySignales() {
		Community community = communityManager.getPostWithSignaledComment();
		return Response.ok(Status.FOUND).entity(community).build();
	}
	
	@GET
	@Path("theUser")
	@Produces(MediaType.APPLICATION_JSON)
	public Response theGoodUser() {
		Long community = communityManager.nombrePost();
		return Response.ok(Status.FOUND).entity(community).build();
	}
	
	@GET
	@Path("nbr")
	@Produces(MediaType.APPLICATION_JSON)
	public Response countPosts() {
		Long community = communityManager.nombrePost();
		return Response.ok(Status.FOUND).entity(community).build();
	}
	
//	@GET
//	@Path("theArtsuki")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response getTheArtsuki() {
//		Artsuki community = communityManager.getTheArtuski();
//		return Response.ok(Status.FOUND).entity(community).build();
//	}
//	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modifyPost(Community community) {
		communityManager.modifyCommunity(community,community.getId());
			return Response.ok("Your Post has been modified!").build();
		
		//return Response.status(Status.NOT_MODIFIED).entity("Project not found or server error!").build();
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCommunity(Community community) {
		if (communityManager.deleteById(community)) {
			return Response.ok("Your Post has been deleted successfully!").build();
		}
		return Response.status(Status.NOT_ACCEPTABLE).entity("Post not found or server error!").build();
	}
	
}