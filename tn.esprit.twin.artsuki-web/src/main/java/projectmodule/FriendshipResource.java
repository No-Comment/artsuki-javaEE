package projectmodule;

import java.util.List;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ejb.artsuki.FriendshipRemote;
import entity.Artsuki;
import entity.Friendship;
import entity.FriendshipStatus;

@Path("friends")
@ManagedBean
public class FriendshipResource {

	@EJB
	private FriendshipRemote friendshipManager;

	@Inject
	@AuthenticatedUser
	Artsuki authenticatedUser;

	@GET
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllFriends()
	{
		
		List<Artsuki> friends=friendshipManager.getAllFriends(authenticatedUser);
		return Response.ok().entity(friends).build();
	}
	

	@GET
	@Path("view")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllFriends2()
	{
		
		List<Friendship> friends=friendshipManager.getAllFriendships(authenticatedUser);
		return Response.ok().entity(friends).build();
	}
	
	
	@GET
	@Secured
	@Path("invitations")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllInvitations()
	{
		
		List<Artsuki> invitations=friendshipManager.getAllInvitations(authenticatedUser);
		return Response.ok().entity(invitations).build();
	}
	
	@POST
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendInvitation(Artsuki toArtsuki)
	{
		try{
		friendshipManager.sendInvitation(authenticatedUser,toArtsuki);
		return Response.ok().build();
		}
		catch(Exception e)
		{
			return Response.status(Status.BAD_REQUEST).entity("Artsuki not found").build();
		}
	}
	
	@PUT
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	public Response respondToInvitation(Friendship friendship)
	{

		try{
		friendshipManager.respondToInvitation(authenticatedUser, friendship);
		return Response.ok().build();
		}
		catch(Exception e)
		{
			return Response.status(Status.BAD_REQUEST).build();
		}
	}
	
	@DELETE
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteFriend(Friendship friendship)
	{
		try{
		friendshipManager.deleteFriend(authenticatedUser, friendship);
			return Response.ok().build();
		}
		catch(Exception e)
		{
			return Response.status(Status.BAD_REQUEST).build();
		}
		}
		
	@DELETE
	@Path("delete")
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteFriendng(@QueryParam("id") String id)
	{
		try{
			Friendship fs=new Friendship();
			fs.setId(Integer.parseInt(id));
		friendshipManager.deleteFriend(authenticatedUser, fs);
			return Response.ok().build();
		}
		catch(Exception e)
		{
			return Response.status(Status.BAD_REQUEST).build();
		}
		}

		
	
}
