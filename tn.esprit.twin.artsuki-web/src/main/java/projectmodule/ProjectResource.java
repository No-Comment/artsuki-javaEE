package projectmodule;

import java.util.List;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ejb.CollaboratorNotFoundException;
import ejb.HasDefaultProjectException;
import ejb.NoSuchCategoryException;
import ejb.NoSuchCollaboratorException;
import ejb.OperationFailedException;
import ejb.ProjectNotFoundException;
import ejb.ProjectRemote;
import entity.Artsuki;
import entity.Project;


@Path("project")
@RequestScoped
public class ProjectResource {
	@EJB
	private ProjectRemote projectManager;
	@Inject
	@AuthenticatedUser
	Artsuki authenticatedUser;
	@Inject
	HttpServletRequest request;
	
	@PUT
	@Path("/seed")
	@Produces(MediaType.APPLICATION_JSON)
	public String seedToDatabase() {
		System.out.println("Project Manager is null ?"+projectManager == null);
		projectManager.addTestUserProject();
		return "Seeding operation is done!";
	} 
	
	@PUT
	@Path("/seedArtworks")
	@Produces(MediaType.APPLICATION_JSON)
	public Response seedArtworksWithRates() throws ProjectNotFoundException {
		projectManager.seedArtworkAndRate();
		return Response.status(Status.ACCEPTED).entity("Oke seeding artworks done").build();
	} 
	
	@POST
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addProjectService(Project project) {
		project.setPublisher(authenticatedUser);
		if( projectManager.addProject(project) ) {
			return Response.ok("{\"message\":\"project Added successfully\"}", MediaType.APPLICATION_JSON).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity(Jsonfy.jsonfy("wrong operation please verify your data")).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProjects( 
			@QueryParam(value="category") String category,
			@QueryParam(value="title") String title,
			@QueryParam(value="keyword")String keyword,
			@QueryParam(value="page") int page,
			@QueryParam(value="numberPage") int nbr
			) {
		if(category != null) {
			try {
				System.out.println("Category : "+category);
				return Response.status(Status.OK).entity(projectManager.findByCategory(category)).build();
			} catch (NoSuchCategoryException e) {
				return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
			}
		}
		
		if(title != null) {
			return Response.status(Status.OK).entity(projectManager.findByTitle(title)).build();
		}
		if(keyword != null) {
			List<Project> projectsFetched = projectManager.getProjectsByKeyword(keyword);
			if (projectsFetched != null) {
				return Response.status(Status.OK).entity(projectsFetched).build();
			}
			return Response.status(Status.NOT_FOUND).entity(Jsonfy.jsonfy("No Result matched with the given keyword!")).build();
		}
		
		if(nbr != 0) return Response.status(Status.OK).entity("{\"number\":\""+projectManager.getProjectNumber()+"\"}").build();
		
		if(page != 0 ) return Response.status(Status.OK).entity(projectManager.getProjects(page)).build();
		return Response.ok( projectManager.getAllProject()).build();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("show")
	public Response getProject(Project project) {
		try {
			Project fetchedProject = projectManager.findById(project.getId());
			projectManager.increaseNumberVisitor(fetchedProject, request.getRemoteAddr());
			return Response.ok(fetchedProject).build();
		} catch (ProjectNotFoundException e) {
			// TODO Auto-generated catch block
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@PUT
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modifyProject(Project project) {
		try {
			Project fetchedProject = projectManager.findById(project.getId());
			if(fetchedProject.getPublisher().getId() == authenticatedUser.getId()) {
				projectManager.modifyProject(project);
				return Response.ok(Jsonfy.jsonfy("Your Project has been modified!")).build();
			}
			return Response.status(Status.NOT_ACCEPTABLE).entity(Jsonfy.jsonfy("Modification denied!")).build();
		} catch (ProjectNotFoundException e) {
			return Response.status(Status.NOT_MODIFIED).entity(e.getMessage()).build();
		}
		
		
	}
	
	@DELETE
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteProject(Project project) {
		try {
			if(project.getPublisher().getId() == authenticatedUser.getId()) {
				projectManager.deleteProject(project) ;
				return Response.ok(Jsonfy.jsonfy("Your Project has been deleted successfully!")).build();
			}
			return Response.status(Status.NOT_ACCEPTABLE).entity(Jsonfy.jsonfy("Your Project has been deleted successfully!")).build();
		} catch (ProjectNotFoundException e) {
			return Response.status(Status.NOT_ACCEPTABLE).entity(e.getMessage()).build();
		}
		
	}
	
	@DELETE
	@Secured
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteProject(@PathParam(value="id") int id) {
		try {
			Project toDeleteProject = projectManager.findById(id);
			if(toDeleteProject.getPublisher().getId() == authenticatedUser.getId()) {
				projectManager.deleteProject(toDeleteProject) ;
				return Response.ok(Jsonfy.jsonfy("Your Project has been deleted successfully!")).build();
			}
			return Response.status(Status.NOT_ACCEPTABLE).entity(Jsonfy.jsonfy("Your Project has been deleted successfully!")).build();
		} catch (ProjectNotFoundException e) {
			return Response.status(Status.NOT_ACCEPTABLE).entity(e.getMessage()).build();
		}
		
	}
	
	@PUT
	@Path("collaborators/add")
	@Secured
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCollaborator(@FormParam(value="projectId") int projectId, @FormParam(value="artsukiId") int artsukiId) {
		try {
			Project fetchedProject = projectManager.findById(projectId);
			if(fetchedProject.getPublisher().getId() == authenticatedUser.getId()) {
				projectManager.addCollaborator(fetchedProject, artsukiId);
				return Response.status(Status.OK).entity(Jsonfy.jsonfy("Collaborators added")).build();
			}
			return Response.status(Status.NOT_ACCEPTABLE).entity(Jsonfy.jsonfy("Operation denied!")).build();
		} catch (ProjectNotFoundException | CollaboratorNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} 
	}
	
	@PUT
	@Path("collaborators/remove")
	@Secured
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeCollaborator(@FormParam(value="projectId") int projectId, @FormParam(value="artsukiId") int artsukiId) {
		try {
			Project fetchedProject = projectManager.findById(projectId);
			if(fetchedProject.getPublisher().getId() == authenticatedUser.getId()) {
				projectManager.removeCollaborator(fetchedProject, artsukiId);
				return Response.status(Status.OK).entity(Jsonfy.jsonfy("Collaborators added")).build();
			}
			return Response.status(Status.NOT_ACCEPTABLE).entity(Jsonfy.jsonfy("Operation denied!")).build();
		} catch (ProjectNotFoundException | CollaboratorNotFoundException | NoSuchCollaboratorException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} 
	}
	
	@POST
	@Path("/collaborators")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProjectCollaborators(Project projectFromRequest) {
		try {
			Project project = projectManager.findById(projectFromRequest.getId());
			if (project.getCollaborators().size() != 0) {
				return Response.status(Status.OK).entity(project.getCollaborators()).build();
			}
			return Response.status(Status.OK).entity(Jsonfy.jsonfy("This Project has no Collaborators!")).build();
		} catch (ProjectNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@POST
	@Path("/tocollaborate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProjectsToCollaborateForMember(Artsuki artsukiFromRequest) {
		try {
			Artsuki artsuki = projectManager.isArtsukiExist(artsukiFromRequest.getId());
			if (artsuki.getProjectsToCollaborate().size() != 0) {
				return Response.status(Status.OK).entity(artsuki.getProjectsToCollaborate()).build();
			}
			return Response.status(Status.OK).entity(Jsonfy.jsonfy("This Artist has no project to collaborate in!")).build();
		} catch (CollaboratorNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@PUT
	@Path("subscribe/add")
	@Secured
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response subscribeToProject(@FormParam(value="project_id")int projectId) {
		try {
			projectManager.subscribeToProject(projectId, authenticatedUser.getId());
			return Response.status(Status.ACCEPTED).entity(Jsonfy.jsonfy("Member has been added to the subscribers list")).build();
		} catch (ProjectNotFoundException | CollaboratorNotFoundException | OperationFailedException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@PUT
	@Path("subscribe/remove")
	@Secured
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response removeSubscription(@FormParam(value="project_id")int projectId) {
		try {
			projectManager.removeSubscribtionToProject(projectId, authenticatedUser.getId());
			return Response.status(Status.ACCEPTED).entity(Jsonfy.jsonfy("Member has been retired from the subscribers list")).build();
		} catch (ProjectNotFoundException | CollaboratorNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@POST
	@Path("subscribers")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getListSubscribersOfProject(Project project) {
		try {
			return Response.status(Status.OK).entity(projectManager.getListSbuscribers(project)).build();
		} catch (ProjectNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@GET
	@Path("subscribers")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNbrSubscribersOfProject(@QueryParam("project_id") int projectId) {
		try {
			return Response.status(Status.OK).entity(Jsonfy.jsonfy(String.valueOf(this.projectManager.numberOfSubscriberss(projectId)))).build();
		} catch (ProjectNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@GET
	@Path("collaborators")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNbrCollaboratorsOfProject(@QueryParam("project_id") int projectId) {
		try {
			return Response.status(Status.OK).entity(Jsonfy.jsonfy(String.valueOf(this.projectManager.numberOfCollaborators(projectId)))).build();
		} catch (ProjectNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@POST
	@Path("subscribed")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getListOfSubscribedProject(Artsuki artsuki) {
		try {
			return Response.status(Status.OK).entity(projectManager.getListOfSubscribe(artsuki)).build();
		} catch (CollaboratorNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@POST
	@Path("default")
	@Secured
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createDefaultProject() {
		try {
			projectManager.createDefaultProject(authenticatedUser);
			return Response.status(Status.CREATED).entity(Jsonfy.jsonfy("Default Project has been Created for the Given user")).build();
		} catch (CollaboratorNotFoundException | HasDefaultProjectException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@POST
	@Path("perUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getProjectsPerUser(Artsuki artsuki) {
		try {
			return Response.status(Status.OK).entity(projectManager.getProjectsPerUser(artsuki.getId())).build();
		} catch (CollaboratorNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	} 
	
	@POST
	@Path("rate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProjectRate(Project project) {
		try {
			return Response.status(Status.OK).entity(projectManager.getProjectRate(project)).build();
		} catch (ProjectNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
	
	@POST
	@Path("views")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNumberOfViews(Project project) {
		try {
			return Response.status(Status.OK).entity(projectManager.numberOfView(project)).build();
		} catch (ProjectNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		}
	}
}
