package projectmodule;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ejb.CommentNotFoundException;
import ejb.CommentRemote;
import ejb.artsuki.ArtsukiRemote;
import entity.Artsuki;
import entity.Comment;

@Path("comments")
@RequestScoped
public class CommentRessource {
	@EJB
	private CommentRemote commentManager;

	@EJB
	private ArtsukiRemote artsukiManager;

	@Inject
	@AuthenticatedUser
	Artsuki authenticatedUser;

	@Secured
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCommentService(Comment comment) {
		Artsuki artsuki = artsukiManager.findArtsukiById(authenticatedUser.getId());
		comment.setCommenter(artsuki);
		if (commentManager.addCommentToArtwork(comment)) {
			return Response.status(Status.CREATED).entity("{\"Message\":\"Your Comment is added successfully!\"}")
					.build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("{\"Message\":\"Missing attributes!\"}").build();
	}

	@Secured
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modifyComment(Comment comment) {
		try {
			Artsuki artsuki = artsukiManager.findArtsukiById(authenticatedUser.getId());
			if (comment != null) {
				commentManager.modifyCommentArtwork(comment, artsuki.getId());
				return Response.ok("{\"Message\":\"Your Comment is updated successfully!\"}").build();
			} else
				return Response.notModified("{\"Message\":\"You are not allowed to update this Comment!\"}").build();
		} catch (CommentNotFoundException e) {
			return Response.status(Status.NOT_MODIFIED).entity(e.getMessage()).build();
		}
	}

	@Secured
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteComment(Comment comment) {
		try {
			Artsuki artsuki = artsukiManager.findArtsukiById(authenticatedUser.getId());
			if (comment != null) {
				commentManager.deleteCommentArtwork(comment, artsuki.getId());
				return Response.ok("{\"Message\":\"Your Comment is deleted successfully!\"}").build();
			} else
				return Response.notModified("{\"Message\":\"You are not allowed to delete this Comment!\"}").build();
		} catch (CommentNotFoundException e) {
			return Response.status(Status.NOT_ACCEPTABLE).entity(e.getMessage()).build();
		}
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCommentsByArtwork(@QueryParam("idArtwork") int idArtwork) {
		List<Comment> comments = commentManager.findByArtwork(idArtwork);
		if (comments.size() != 0) {
			return Response.status(Status.ACCEPTED).entity(comments).build();
		} else
			return Response.status(Response.Status.BAD_REQUEST).entity("{\"Message\":\"Empty List!\"}").build();
	}

}
