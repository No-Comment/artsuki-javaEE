package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement


public class Event extends Publication implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7500796361015915769L;
	
	private Timestamp dateEnd;
	
	private Timestamp dateStart;
	private String description;
	private int nbSeats;
	private String photo;
	private String places;
	@ManyToMany(mappedBy="event",fetch=FetchType.EAGER,cascade={CascadeType.REMOVE})
	private List<Artsuki> participant;
	@Enumerated(EnumType.STRING)
	private EventCategory category;
	@Enumerated(EnumType.STRING)
	private EventType type;
	private int etat=1;
	
	



	public Event() {
		super();
	}
	
	public int getEtat() {
		return etat;
	}


	public void setEtat(int etat) {
		this.etat = etat;
	}
	@Temporal(TemporalType.TIMESTAMP)
	public Timestamp getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(Timestamp dateEnd) {
		this.dateEnd = dateEnd;
	}
	@Temporal(TemporalType.TIMESTAMP)
	public Timestamp getDateStart() {
		return dateStart;
	}
	public void setDateStart(Timestamp dateStart) {
		this.dateStart = dateStart;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getNbSeats() {
		return nbSeats;
	}
	public void setNbSeats(int nbSeats) {
		this.nbSeats = nbSeats;
	}

	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	public List<Artsuki> getParticipant() {
		return participant;
	}
	public void setParticipant(List<Artsuki> participant) {
		this.participant = participant;
	}
	public EventCategory getCategory() {
		return category;
	}
	public void setCategory(EventCategory category) {
		this.category = category;
	}
	public String getPlaces() {
		return places;
	}
	public void setPlaces(String places) {
		this.places = places;
	}



	public EventType getType() {
		return type;
	}



	public void setType(EventType type) {
		this.type = type;
	}

	public void addParticipant(Artsuki participant) {
		if (this.participant == null) {
			this.participant = new ArrayList<>();
		}
		this.participant.add(participant);
	}
	
}
