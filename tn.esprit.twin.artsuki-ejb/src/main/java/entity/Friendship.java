package entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Friendship
 *
 */
@Entity
public class Friendship implements Serializable {
	
	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	private Artsuki Requester;
	
	@ManyToOne
	private Artsuki Receiver;
	
	@Enumerated(EnumType.STRING)
	private FriendshipStatus status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Artsuki getRequester() {
		return Requester;
	}
	public void setRequester(Artsuki requester) {
		Requester = requester;
	}
	public Artsuki getReceiver() {
		return Receiver;
	}
	public void setReceiver(Artsuki receiver) {
		Receiver = receiver;
	}
	public FriendshipStatus getStatus() {
		return status;
	}
	public void setStatus(FriendshipStatus status) {
		this.status = status;
	}

	
}
