package entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Comment implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String content;
	@JsonIgnore
	private int nbrSignla = 0;
	@ManyToOne
	private Publication publication;
	@ManyToOne
	@JsonIgnore
	private Artsuki commenter;
	
	@ManyToOne
	@JoinColumn(name="comment_fk")
	@JsonIgnore
	private Comment reComment;
	
	@OneToMany(mappedBy="reComment", fetch= FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Comment> comments;

	public Comment() {
		super();
	}
	
	public Comment(int id, String content, int nbrSignla, Publication publication, Artsuki commenter) {
		super();
		this.id = id;
		this.content = content;
		this.nbrSignla = nbrSignla;
		this.publication = publication;
		this.commenter = commenter;
	}
	
	public Comment(String content, int nbrSignla, Publication publication) {
		super();
		this.content = content;
		this.nbrSignla = nbrSignla;
		this.publication = publication;
	}

	public Comment(String content, int nbrSignla) {
		super();
		this.content = content;
		this.nbrSignla = nbrSignla;
	}

	public Comment(String content, int nbrSignla, Publication publication, Artsuki commenter) {
		super();
		this.content = content;
		this.nbrSignla = nbrSignla;
		this.publication = publication;
		this.commenter = commenter;
	}
	
	public Comment(String content, Publication publication, Artsuki commentator) {
		super();
		this.content = content;
		this.publication = publication;
		this.commenter = commentator;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getNbrSignla() {
		return nbrSignla;
	}
	public void setNbrSignla(int nbrSignla) {
		this.nbrSignla = nbrSignla;
	}

	public Publication getPublication() {
		return publication;
	}

	public void setPublication(Publication publication) {
		this.publication = publication;
	}

	public Artsuki getCommenter() {
		return commenter;
	}

	public void setCommenter(Artsuki commenter) {
		this.commenter = commenter;
	}

	public Comment getReComment() {
		return reComment;
	}

	public void setReComment(Comment reComment) {
		this.reComment = reComment;
	}
	
	@XmlTransient
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	@Override
	public String toString() {
		return "[id=" + id + ", content=" + content + ", nbrSignal=" + nbrSignla + "]";
	}
	
}
