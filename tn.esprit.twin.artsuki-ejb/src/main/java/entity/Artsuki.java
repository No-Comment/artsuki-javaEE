package entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@XmlRootElement
public class Artsuki implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String firstname;
	private String lastname;
	private String nationality;
	private String password;
	private String phoneNumber;
	private String username;
	private String gender;
	private String email;
	@Enumerated(EnumType.STRING)
	private Country country;
	private String city;
	private String street;
	private String town;
	private boolean isVerified;
	@Enumerated(EnumType.STRING)
	private Role role;
	private String biography;
	@Enumerated(EnumType.STRING)
	private Art art;
	@ManyToMany(mappedBy="subscribers",fetch=FetchType.EAGER)
	private Set<Project> subscribedProjects;
	@ManyToMany(fetch=FetchType.EAGER)
	private Set<Publication> bookmarks;
	@ManyToMany(fetch=FetchType.EAGER)
	private Set<Project> projectsToCollaborate;
	@ManyToMany(fetch=FetchType.EAGER,cascade={CascadeType.REMOVE})
	private Set<Event> event;
	
	@OneToMany(mappedBy="Requester",fetch=FetchType.EAGER)
	private Set<Friendship> sent;
	
	@OneToMany(mappedBy="Receiver",fetch=FetchType.EAGER)
	private Set<Friendship> invitations;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	@XmlTransient
	public Set<Project> getSubscribedProjects() {
		return subscribedProjects;
	}
	public void setSubscribedProjects(Set<Project> subscribedProjects) {
		this.subscribedProjects = subscribedProjects;
	}

	@XmlTransient
	public Set<Publication> getBookmarks() {
		return bookmarks;
	}
	public void setBookmarks(Set<Publication> bookmarks) {
		this.bookmarks = bookmarks;
	}
	public Art getArt() {
		return art;
	}
	public void setArt(Art art) {
		this.art = art;
	}
	
	
	@XmlTransient
	public Set<Friendship> getSent() {
		return sent;
	}
	public void setSent(Set<Friendship> sent) {
		this.sent = sent;
	}
	@XmlTransient
	public Set<Friendship> getInvitations() {
		return invitations;
	}
	public void setInvitations(Set<Friendship> invitations) {
		this.invitations = invitations;
	}
	public boolean isVerified() {
		return isVerified;
	}
	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}
	@XmlTransient
	public Set<Project> getProjectsToCollaborate() {
		return projectsToCollaborate;
	}
	public void setProjectsToCollaborate(Set<Project> projectsToCollaborate) {
		this.projectsToCollaborate = projectsToCollaborate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artsuki other = (Artsuki) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public void addProjectToCollaborate(Project project) {
		if(this.projectsToCollaborate == null) {
			this.projectsToCollaborate = new HashSet<>();
		}
		this.projectsToCollaborate.add(project);
		project.addCollaborator(this);
	}
	
	public void removeProjectToCollaborate(Project project) {
		if(this.projectsToCollaborate == null) {
			this.projectsToCollaborate = new HashSet<>();
		}
		this.projectsToCollaborate.remove(project);
		project.removeCollaborator(this);
	}
	
	public void addSubscribedProject(Project project) {
		if(this.subscribedProjects == null) {
			this.subscribedProjects = new HashSet<>();
		}
		this.subscribedProjects.add(project);
	}
	
	public void removeSubscriber(Project project) {
		if (this.subscribedProjects != null) {
			this.subscribedProjects.remove(project);
		}
	}
	public void addEventParticipe(Event evnt) {
		if(this.event == null) {
			this.event = new HashSet<>();
		}
		this.event.add(evnt);
		evnt.addParticipant(this);
	}
	@XmlTransient
	public Set<Event> getEvent() {
		return event;
	}
	public void setEvent(Set<Event> event) {
		this.event = event;
	}
}
