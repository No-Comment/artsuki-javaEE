package entity;

public enum CategoryArtwork {
	ARCHITECTURAL_MODEL,
	SCULPTURE,
	PAINTING,
	MUSIC,
	POEM,
	BOOK,
	PLAY,
	TRAILER,
	PHOTO,
	COMICS
}
