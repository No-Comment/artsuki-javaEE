package entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@XmlRootElement
public class Artwork extends Publication implements Serializable {
	@Enumerated(EnumType.STRING)
	@Column(name = "artwork_category")
	private CategoryArtwork category;
	private String description;
	@Column(name = "media_type")
	private String mediaType;
	@Column(name = "media_path")
	private String mediaPath;
	private boolean forSale = true;
	private double price;
	@ManyToOne
	@JsonIgnore
	private Project project ;

	public Artwork() {
		super();
	}

	public Artwork(Timestamp datePub, String title, PublicationType publicationType, Artsuki publisher,
			String description, boolean forSale, CategoryArtwork category, double price) {
		super.datePub = datePub;
		super.title = title;
		super.publicationType = publicationType;
		super.publisher = publisher;
		this.description = description;
		this.forSale = forSale;
		this.category = category;
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isForSale() {
		return forSale;
	}

	public void setForSale(boolean forSale) {
		this.forSale = forSale;
	}

	public CategoryArtwork getCategory() {
		return category;
	}

	public void setCategory(CategoryArtwork category) {
		this.category = category;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getMediaPath() {
		return mediaPath;
	}

	public void setMediaPath(String mediaPath) {
		this.mediaPath = mediaPath;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	@Override
	public String toString() {
		return super.toString() + "\nCategory = " + category + ", Description = " + description
				+ ", Media Type = " + mediaType + ", Media Path = " + mediaPath + ", For Sale = " + forSale + ", Price = "
				+ price + "$]\n";
	}

}
