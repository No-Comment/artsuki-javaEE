package entity;

public enum Media {
	SOUND,
	VISUAL,
	TEXT,
	PHOTO,
	VIDEO,
	AUDIO
}
