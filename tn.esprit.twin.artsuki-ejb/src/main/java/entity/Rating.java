package entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Rating implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private float value;
	@ManyToOne
	private Publication publication;
	@ManyToOne
	private Artsuki rater;
	
	public Rating() {
		super();
	}
	
	public Rating(float value) {
		super();
		this.value = value;
	}
	
	public Rating(float value, Publication publication, Artsuki rater) {
		super();
		this.value = value;
		this.publication = publication;
		this.rater = rater;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}
	public Publication getPublication() {
		return publication;
	}
	public void setPublication(Publication publication) {
		this.publication = publication;
	}
	public Artsuki getRater() {
		return rater;
	}
	public void setRater(Artsuki rater) {
		this.rater = rater;
	}
	
	@Override
	public String toString() {
		return "[value=" + value + "]";
	}
	
	
}
