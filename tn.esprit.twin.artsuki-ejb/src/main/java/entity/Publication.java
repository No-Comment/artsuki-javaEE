package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@XmlRootElement
public class Publication implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	protected Timestamp datePub;
	protected String title;
	@Enumerated(EnumType.STRING)
	protected PublicationType publicationType;
	@ManyToOne
	protected Artsuki publisher;
	@OneToMany(mappedBy = "publication", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected List<Comment> comments = new ArrayList<Comment>();
	@OneToMany(mappedBy = "publication", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected List<Rating> rates = new ArrayList<Rating>();

	public Publication() {
		super();
	}
	
	public Publication(Timestamp datePub, String title, PublicationType publicationType, Artsuki publisher) {
		super();
		this.datePub = datePub;
		this.title = title;
		this.publicationType = publicationType;
		this.publisher = publisher;
	}
	
	

	public Publication(Timestamp datePub, String title, PublicationType publicationType) {
		super();
		this.datePub = datePub;
		this.title = title;
		this.publicationType = publicationType;
	}
	@Temporal(TemporalType.TIMESTAMP)
	public Timestamp getDatePub() {
		return datePub;
	}
	public void setDatePub(Timestamp datePub) {
		this.datePub = datePub;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public PublicationType getPublicationType() {
		return publicationType;
	}
	public void setPublicationType(PublicationType publicationType) {
		this.publicationType = publicationType;
	}
	public Artsuki getPublisher() {
		return publisher;
	}
	public void setPublisher(Artsuki publisher) {
		this.publisher = publisher;
	}

	@XmlTransient
	public List<Rating> getRates() {
		return rates;
	}

	public void setRates(List<Rating> rates) {
		this.rates = rates;
	}

	@XmlTransient
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "Publication [id=" + id + ", title=" + title + ", datePub=" + datePub + ", publicationType="
				+ publicationType + ",  \n publisher=" + publisher + ", \n comments=" + comments + ", \n rates=" + rates
				+ "";
	}
}
