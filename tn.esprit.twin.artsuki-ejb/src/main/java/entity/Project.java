package entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@XmlRootElement
public class Project implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String description;
	private String photo;
	private String title;
	private long numberOfView;
	@Enumerated(EnumType.STRING)
	@Column(name="project_category")
	private CategoryProject category;
	@ManyToOne
	private Artsuki publisher;
	@ManyToMany(fetch=FetchType.EAGER)
	private Set<Artsuki> subscribers;
	@ManyToMany(mappedBy="projectsToCollaborate", fetch=FetchType.EAGER)
	private Set<Artsuki> collaborators;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public CategoryProject getCategory() {
		return category;
	}
	public void setCategory(CategoryProject category) {
		this.category = category;
	}
	public Artsuki getPublisher() {
		return publisher;
	}
	public void setPublisher(Artsuki publisher) {
		this.publisher = publisher;
	}
	@XmlTransient
	public Set<Artsuki> getSubscribers() {
		return subscribers;
	}
	public void setSubscribers(Set<Artsuki> subscribers) {
		this.subscribers = subscribers;
	}
	@XmlTransient
	public Set<Artsuki> getCollaborators() {
		return collaborators;
	}
	public void setCollaborators(Set<Artsuki> collaborators) {
		this.collaborators = collaborators;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (id != other.id)
			return false;
		return true;
	}	
	public void addCollaborator(Artsuki artsuki) {
		if (this.collaborators == null) {
			this.collaborators = new HashSet<>();
		}
		this.collaborators.add(artsuki);
	}
	
	public void removeCollaborator(Artsuki artsuki) {
		if (this.collaborators == null) {
			this.collaborators = new HashSet<>();
		}
		this.collaborators.remove(artsuki);
	}
	
	public void addSubscriber(Artsuki artsuki) {
		if (this.subscribers == null) {
			this.subscribers = new HashSet<>();
		}
		this.subscribers.add(artsuki);
		artsuki.addSubscribedProject(this);
	}
	
	public void removeSubscriber(Artsuki artsuki) {
		if(this.subscribers != null) {
			this.subscribers.remove(artsuki);
			artsuki.removeSubscriber(this);
		}
	}
	public long getNumberOfView() {
		return numberOfView;
	}
	public void setNumberOfView(long numberOfView) {
		this.numberOfView = numberOfView;
	}
	
	
}
