package entity;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@XmlRootElement
public class Community extends Publication implements Serializable {
	private String attachement;
	private String message;
	private String theme;
	
	
	
	public Community() {
		super();
	}

	public Community(Timestamp datePub, String title, PublicationType publicationType, String attachement, String message, String theme) {
		super(datePub,title, publicationType);
		this.attachement=attachement;
		this.message=message;
		this.theme=theme;
	}

	public Community(Timestamp datePub, String title, PublicationType publicationType, Artsuki publisher, String attachement, String message, String theme) {
		super(datePub, title, publicationType, publisher);
		this.attachement=attachement;
		this.message=message;
		this.theme=theme;
	}
	
	

	public Community(String attachement, String message, String theme) {
		super();
		this.attachement = attachement;
		this.message = message;
		this.theme = theme;
	}

	public String getAttachement() {
		return attachement;
	}
	public void setAttachement(String attachement) {
		this.attachement = attachement;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	
}
