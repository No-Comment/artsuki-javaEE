package ejb;

public class ArtworkNotFoundException extends Exception{
	public ArtworkNotFoundException(){
		super("Artwork Not Found");
	}

}
