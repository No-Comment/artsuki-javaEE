package ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import entity.Artsuki;
import entity.Comment;

@Stateless
public class CommentEJB implements CommentRemote, CommentLocal {
	@PersistenceContext(unitName = "tn.esprit.twin.artsuki-ejb")
	EntityManager em;

	@Override
	public int addComment(Comment comment) {
		Artsuki artsuki = em.find(Artsuki.class, comment.getId());
		comment.setCommenter(artsuki);
		em.persist(comment);
		return comment.getId();
	}

	@Override
	public void modifyComment(Comment comment, int id) {
		Comment currentComment = em.find(Comment.class, id);
		if (currentComment != null) {
			currentComment.setContent(comment.getContent());
			em.merge(currentComment);
		}

	}

	@Override
	public void deleteComment(int id) {
		em.remove(em.find(Comment.class, id));

	}

	@Override
	public boolean deleteById(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Comment> getAllComment() {
		return em.createQuery("SELECT c from Comment c").getResultList();
	}

	@Override
	public Comment findById(int id) {
		TypedQuery<Comment> query = em.createQuery("select c from Comment c where id=:id", Comment.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	@Override
	public Comment findThatComment() {
		Query query = em.createQuery("SELECT MAX(c.nbrSignla) FROM Comment c");
		int flo = (int) query.getSingleResult();
		Query query2 = em.createQuery("SELECT r.id FROM Comment r where r.nbrSignla=:flo");
		query2.setParameter("flo", flo);
		int idP = (int) query2.getSingleResult();
		Query query1 = em.createQuery("SELECT c FROM Comment c Where c.id=:idP");
		query1.setParameter("idP", idP);
		return (Comment) query1.getSingleResult();
	}
	
	// <----- Artwork Module--------->
	@Override
	public boolean addCommentToArtwork(Comment comment) {
		em.persist(comment);
		return true;
	}

	@Override
	public boolean modifyCommentArtwork(Comment comment, int idCurrentUser) throws CommentNotFoundException {
		if (comment != null) {
			Comment currentComment = findCommentById(comment.getId());
			if (currentComment.getCommenter().getId() == idCurrentUser) {
				currentComment.setContent(comment.getContent());
				em.merge(currentComment);
				return true;
			}
			throw new CommentNotFoundException();
		} else
			return false;
	}

	@Override
	public boolean deleteCommentArtwork(Comment comment, int idCurrentUser) throws CommentNotFoundException {
		if (comment != null) {
			Comment currentComment = findCommentById(comment.getId());
			if (currentComment.getCommenter().getId() == idCurrentUser) {
				em.remove(currentComment);
				return true;
			}
			throw new CommentNotFoundException();
		} else
			return false;
	}

	@Override
	public List<Comment> findByArtwork(int id) {
		TypedQuery<Comment> query = em.createQuery("FROM Comment WHERE publication_id =:id", Comment.class);
		query.setParameter("id", id);
		List<Comment> comments = query.getResultList();
		return comments;
	}

	@Override
	public Comment findCommentById(int id) {
		return em.find(Comment.class, id);
	}
}