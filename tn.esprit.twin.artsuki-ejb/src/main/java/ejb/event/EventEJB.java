package ejb.event;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ejb.ProjectNotFoundException;
import entity.Art;
import entity.Artsuki;
import entity.Country;
import entity.Event;
import entity.EventCategory;
import entity.EventType;
import entity.Project;
import entity.PublicationType;
import entity.Rating;
import entity.Role;


@Stateless
public class EventEJB implements EventLocal,EventRemote{
	
	@PersistenceContext(unitName="tn.esprit.twin.artsuki-ejb")
	EntityManager em;
	


	@Override
	public boolean addEvent(Event event) {
		event.setPublicationType(PublicationType.Event);	
		//Calendar cal = Calendar.getInstance();
		
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		if(event.getType()==EventType.unlimitedSeats) {
			
			event.setNbSeats(0);
		}
		if (event.getDateStart().compareTo(event.getDateEnd())==-1)
		{
		if(event.getDateEnd().compareTo(timestamp)==1) {

	event.setDatePub(timestamp);
		System.out.println("Event added");

		em.persist(event);
		return true;
		}else{System.out.println("SysDate can not be higher than end date");}
		}else{System.out.println("Start date can not be higher than end date");}
		return false;
	}

	
	

	@Override
	public List<Event> displayEvent() {
		
		return em.createQuery("SELECT e from Event e",Event.class).getResultList();
	}
	
	@Override
	public List<Event> displayEventArchived() {
		return em.createQuery("SELECT e from Event e where e.etat=0",Event.class).getResultList();

	}
	@Override
	public List<Event> displayEventAvailable() {
		return em.createQuery("SELECT e from Event e where e.etat=1",Event.class).getResultList();
	}
	@Override
	public List<Event> displayOrder() {
		return em.createQuery("SELECT e.description from Event e where e.etat=1 ORDER BY e.datePub DESC",Event.class).getResultList();
	}
	@Override
	public int ParticipantExist(Event e,Artsuki a) {
		Artsuki art = em.find(Artsuki.class, a.getId());
		Event event = em.find(Event.class, e.getId());
		for(Artsuki ev:event.getParticipant()) {
			if(art.getId()==ev.getId()) {
				return 2;
			}

		}
		return 0;
	}
	
	@Override
	public boolean removeEvent(int event) {
      Event evnt = em.find(Event.class, event);
		if (evnt != null) {
			em.remove(evnt);
			System.out.println("Event Deleted");
			return true;
		}
		return false;
	}
	@Override
	public List<Artsuki> displayParticipant(Event event)  {
		Event evt = em.find(Event.class,event.getId());
		if (evt != null) {
			return evt.getParticipant();
		}
		return null;
	}


	@Override
	public boolean updateEvent(Event event)  {
		Event evnt = em.find(Event.class, event.getId());
		if (evnt != null) {
			evnt.setCategory(event.getCategory());
			evnt.setType(event.getType());

			evnt.setDescription(event.getDescription());
			evnt.setPhoto(event.getPhoto());
			evnt.setTitle(event.getTitle());
			evnt.setDateStart(event.getDateStart());
			evnt.setDateEnd(event.getDateEnd());
			evnt.setPlaces(event.getPlaces());
			evnt.setPublicationType(PublicationType.Event);
			//evnt.setDatePub(event.getDatePub());
			if(evnt.getType()==EventType.unlimitedSeats) {
				event.setNbSeats(0);
			}else {	evnt.setNbSeats(event.getNbSeats());}
			 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				
				if (event.getDateStart().compareTo(event.getDateEnd())==-1)
				{
				if(event.getDateEnd().compareTo(timestamp)==1) {
			em.merge(evnt);		
			System.out.println("Event Updated");

			//em.createQuery("SELECT e from Event e where id= :id",Event.class).setParameter("id", event.getId()).getSingleResult();

			return true;
				}else{System.out.println("SysDate can not be higher than end date");}
				}else{System.out.println("Start date can not be higher than end date");}
		}
		return false;
	}

	

	@Override
	public boolean addParticipants(int eventId,int participantId) {
		Event event=em.find(Event.class, eventId);
		Artsuki artsuki=em.find(Artsuki.class, participantId);
		if(event != null && ((event.getNbSeats()>0 && event.getType()==EventType.limitedSeats)|| event.getType()==EventType.unlimitedSeats ) ){
			int nbseat=event.getNbSeats() - 1;
			System.out.println(nbseat);
			event.setNbSeats(nbseat);
				if(artsuki!=null){
					artsuki.addEventParticipe(event);
					System.out.println("Artsuki Added to this Event");

					em.merge(event);
					return true;
				}System.out.println("can not find this Artsuki");
			
		}System.out.println(" Event not available");
		return false;
	}
	
	/*@Override
	public void affecterDepartementAEntreprise(long deptId, long entrepriseId) {
		
		Event departement = em.find(Event.class, deptId);
		Artsuki entreprise = em.find(Artsuki.class, entrepriseId);
		if ((departement!=null) && (entreprise!=null)){
			entreprise.addEventParticipe(departement);
			em.merge(departement);
		}
		
	}*/

   @Override
   public boolean AddRateArtsuki()
   {
	   Artsuki artsuki = new Artsuki();
	   Artsuki artsuki1 = new Artsuki();

	   //rate.setId(1);
	
	   //rate.setPublication(1);
	   
	   artsuki.setArt(Art.MUISC);
	   artsuki.setBiography("bio");
	   artsuki.setCity("ariana");
	   artsuki.setEmail("ahmed@esprit.tn");
	   artsuki.setCountry(Country.Algeria);
	   artsuki.setFirstname("ahmed");
	   artsuki.setGender("fs");
	   artsuki.setLastname("ahmed");
	   artsuki.setNationality("tunisian");
	   artsuki.setPassword("123");
	   artsuki.setPhoneNumber("5555555");
	   artsuki.setRole(Role.ARTIST);
	   artsuki.setStreet("dsq");
	   artsuki.setTown("ds");
	   artsuki.setUsername("ahmed");
	   
	   
	   
	   artsuki1.setArt(Art.PAINTING);
	   artsuki1.setBiography("bio");
	   artsuki1.setCity("tunis");
	   artsuki1.setEmail("zied@esprit.tn");
	   artsuki1.setCountry(Country.France);
	   artsuki1.setFirstname("zied");
	   artsuki1.setGender("fs");
	   artsuki1.setLastname("ahmed");
	   artsuki1.setNationality("tunisian");
	   artsuki1.setPassword("123");
	   artsuki1.setPhoneNumber("5555555");
	   artsuki1.setRole(Role.ARTIST);
	   artsuki1.setStreet("dsq");
	   artsuki1.setTown("ds");
	   artsuki1.setUsername("zied");
	   
	   //em.persist(artsuki);	
	   //em.persist(artsuki1);	
	   
	   Rating rate =new Rating();

	   rate.setValue(4);
	 
	   em.persist(rate);
	   return true;
   }

	
	@Override
	public double AvgRate(Event event) {
		
        //Event evt=em.find(Event.class, event.getId());
        Query rate = em.createQuery("SELECT SUM(r.value)/COUNT(r.publication) FROM Rating r JOIN r.publication p WHERE p.id = :event");
        rate.setParameter("event", event.getId());
		return (double) rate.getSingleResult();
	}
	
	@Override
	public List<Event> findEventByCategory(EventCategory category)  {
	
		return em.createQuery("SELECT e from Event e WHERE e.category like :param", Event.class)
				.setParameter("param","%"+category+"%").getResultList();

	}
	@Override
	public List<Event> findEventByPlace(String place)  {
	
		return em.createQuery("SELECT e from Event e WHERE e.places like :param", Event.class)
				.setParameter("param","%"+place+"%").getResultList();

	}
	@Override
	public List<Event> findEventByDate(Timestamp date1)  {
	
		return em.createQuery("SELECT e from Event e WHERE :param BETWEEN e.dateStart AND e.dateEnd", Event.class)
				.setParameter("param",date1).getResultList();

	}
	@Override
	public Event getIdEvent(int id)  {
		Event evnt = em.find(Event.class, id);
		if (evnt != null) 
			return evnt;
		
		return null;
	}


	@Override
	public boolean Archive(Event e) {
		Event evt = em.find(Event.class, e.getId());
		//Calendar cal = Calendar.getInstance();
		 Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		if ((evt.getDateEnd().compareTo(timestamp)==-1) || (evt.getNbSeats()==0 && evt.getType()==EventType.limitedSeats))
		{
		e.setEtat(0);
		em.merge(e);
		System.out.println("Done");

		return true;
		}else {
		return false;
		}

	}
	@Override
	public boolean addRate(Rating rating) {
		em.persist(rating);
		return true;
	}
	
	
}
