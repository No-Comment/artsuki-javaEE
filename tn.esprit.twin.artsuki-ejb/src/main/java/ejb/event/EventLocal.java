package ejb.event;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Local;

import entity.Artsuki;
import entity.Event;
import entity.EventCategory;
import entity.Rating;


@Local
public interface EventLocal {
	
	public boolean addEvent(Event event) ;
	public List<Event> displayEvent();
	public boolean removeEvent(int event);
	public boolean updateEvent(Event event);
	public boolean addParticipants(int eventId,int participant);
	public List<Event> findEventByCategory(EventCategory category);
	//public boolean removeEventById(int eventid);
	//public boolean updateEventById(Event event);
	//public List<Event> findById(int id);
	public List<Event> displayEventArchived();
	public List<Event> displayEventAvailable();
	public boolean Archive(Event e);
	public Event getIdEvent(int id);
	public int ParticipantExist(Event e,Artsuki a);
	public List<Artsuki> displayParticipant(Event event) ;
	public double AvgRate(Event event);
    public boolean AddRateArtsuki();
    public boolean addRate(Rating rating);
    public List<Event> displayOrder();
    public List<Event> findEventByDate(Timestamp date1);
    public List<Event> findEventByPlace(String place);
	}
