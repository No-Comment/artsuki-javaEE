package ejb;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entity.Art;
import entity.Artsuki;
import entity.Artwork;
import entity.CategoryProject;
import entity.Media;
import entity.Project;
import entity.PublicationType;
import entity.Rating;
import entity.Role;
import entity.Visitor;

@Stateless
public class ProjectEJB implements ProjectRemote, ProjectLocal {
	@PersistenceContext
	EntityManager em;

	@Override
	public boolean addProject(Project project) {
		try {
			Artsuki currentArtsuki = isArtsukiExist(project.getPublisher().getId());
			project.setPublisher(currentArtsuki);
			em.persist(project);
			return true;
		} catch (CollaboratorNotFoundException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	@Override
	public boolean modifyProject(Project project) throws ProjectNotFoundException {
		Project currentProject = em.find(Project.class, project.getId());
		if (currentProject != null) {
			currentProject.setCategory(project.getCategory());
			currentProject.setDescription(project.getDescription());
			currentProject.setPhoto(project.getPhoto());
			currentProject.setTitle(project.getTitle());
			em.merge(currentProject);
			return true;
		}
		throw new ProjectNotFoundException();
	}

	@Override
	public boolean deleteProject(Project project) throws ProjectNotFoundException{
		Project currentProject = em.find(Project.class, project.getId());
		if (currentProject != null) {
			Query query = em.createQuery("delete from Visitor v where v.project = :project");
			query.setParameter("project", project);
			query.executeUpdate();
			query = em.createNativeQuery("delete from project_artsuki where subscribedProjects_id = :id");
			query.setParameter("id", project.getId());
			query.executeUpdate();
			query = em.createNativeQuery("delete from artsuki_project where projectsToCollaborate_id = :id");
			query.setParameter("id", project.getId());
			query.executeUpdate();
			em.remove(currentProject);
			return true;
		}
		throw new ProjectNotFoundException();
	}

	@Override
	public boolean addCollaborator(Project project, int artsukiId) throws CollaboratorNotFoundException, ProjectNotFoundException {
		Artsuki collaborator;
		if(project != null) {
			if(project.getPublisher().getId() != artsukiId) {
				collaborator = isArtsukiExist(artsukiId);
				collaborator.addProjectToCollaborate(project);
				em.merge(project);
				return true;
			}
			return false;
		}
		throw new ProjectNotFoundException();
	}
	
	public Artsuki isArtsukiExist(int id) throws CollaboratorNotFoundException { 
		Artsuki artsuki = em.find(Artsuki.class, id);
		if (artsuki != null) {
			return artsuki;
		}
		throw new CollaboratorNotFoundException();
	}

	@Override
	public boolean removeCollaborator(Project project, int artsukiId) throws ProjectNotFoundException, CollaboratorNotFoundException {
		Artsuki collaborator;
		if(project != null) {
			if(project.getPublisher().getId() != artsukiId) {
				collaborator = isArtsukiExist(artsukiId);
				collaborator.removeProjectToCollaborate(project);
				em.merge(project);
			return true;
			}
			return false;
		}
		throw new ProjectNotFoundException();
	}

	@Override
	public Project findById(int id) throws ProjectNotFoundException {
		Project project = em.find(Project.class, id);
		if (project != null) 
			return project;
		throw new ProjectNotFoundException();
	}

	@Override
	public List<Project> findByCategory(String category) throws NoSuchCategoryException{
		try {
			CategoryProject cat = CategoryProject.valueOf(category.trim());
			Query searchQuery = em.createQuery("FROM Project p WHERE p.category = :category");
			searchQuery.setParameter("category", cat);
			return searchQuery.getResultList();
		}catch (IllegalArgumentException e) {
			throw new NoSuchCategoryException();
		}
	}

	@Override
	public List<Project> findByTitle(String title) {
		Query searchQuery = em.createQuery("FROM Project p WHERE p.title like :title");
		searchQuery.setParameter("title", "%"+title.trim()+"%");
		return searchQuery.getResultList();
	}

	@Override
	public List<Project> getAllProject() {
		return em.createQuery("from Project p order by p.id desc").getResultList();
	}
	
	public List<Project> getProjects(int page) {
		 Query query  = em.createQuery("from Project p order by p.id desc");
		 query.setFirstResult((page-1)*10+1);
		 query.setMaxResults(10);
		 return query.getResultList();
	}
	
	public long getProjectNumber() {
		 return (long) em.createQuery("select count(p.id) from Project p").getSingleResult();
	}

	public void addTestUserProject() {
		Artsuki artsuki2;
		List<Artsuki> artsukis = new ArrayList<>();
		for(int i = 5; i<10 ; i++) {
			artsuki2 = new Artsuki();
			artsuki2.setArt(Art.PAINTING);
			artsuki2.setBiography("Lorem ipsum kitum parelm sahem tarem pam bangalo "+i);
			artsuki2.setEmail("artsuki"+i+"@artsuki.com");
			artsuki2.setUsername("user"+i);
			artsuki2.setFirstname("test-user"+i);
			artsuki2.setLastname("test-user"+i);
			artsuki2.setGender("male");
			artsuki2.setPassword("753357");
			artsuki2.setRole(Role.ARTIST);
			artsuki2.setVerified(true);
			artsukis.add(artsuki2);
		}
		
		for (Artsuki artsuki3 : artsukis) {
			em.persist(artsuki3);
		}
		Project p1 = new Project();
		p1.setCategory(CategoryProject.ANIME_MANGA);
		p1.setTitle("Nartuo");
		p1.setPhoto("Helle.jpg");
		p1.setPublisher(artsukis.get(0));
		em.persist(p1);
		
		 Project p2 = new Project();
		 Project p3 = new Project();
		 Project p4 = new Project();
		 Project p5 = new Project();
		 
			p2.setTitle("Mother's Day 2030");
			p2.setPhoto("kitty.jpg");
			p2.setCategory(CategoryProject.DIGITAL_ART);
			p2.setDescription("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat");
			p2.setPublisher(artsukis.get(0));
			em.persist(p2);
			
			p3.setCategory(CategoryProject.PAINTING);
			p3.setTitle("LaJaconde 2020");
			p3.setPhoto("lajaconde.jpg");
			p3.setDescription("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat");
			p3.setPublisher(artsukis.get(1));
			em.persist(p3);
			
			p4.setCategory(CategoryProject.ANIME_MANGA);
			p4.setTitle("The Best Hoster x27");
			p4.setPhoto("hoster.jpg");
			p4.setDescription("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat");
			p4.setPublisher(artsukis.get(4));
			em.persist(p4);
			
			p5.setCategory(CategoryProject.ANIME_MANGA);
			p5.setTitle("Dragon ball Super X78");
			p5.setPhoto("hawk.jpg");
			p5.setDescription("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat");
			p5.setPublisher(artsukis.get(2));
			em.persist(p5);
			
	}
	
	public List<Project> getProjectsByKeyword(String keyword) {
		Query searchQuery = em.createQuery("FROM Project p "
				+ "WHERE p.title LIKE '%"+keyword+"%'"
				+ "OR p.description LIKE '%"+keyword+"%'");
		return searchQuery.getResultList();
	}

	@Override
	public boolean subscribeToProject(int projectId, int userId) throws ProjectNotFoundException, CollaboratorNotFoundException, OperationFailedException {
		Project currentProject = findById(projectId);
		if (currentProject != null) {
				Artsuki fetchedArtsuki = isArtsukiExist(userId);
				if (fetchedArtsuki == null ) throw new CollaboratorNotFoundException();
				if (fetchedArtsuki.getId() != currentProject.getPublisher().getId()) {
					currentProject.addSubscriber(fetchedArtsuki);
					em.merge(currentProject);
					return true;
				}
				throw new OperationFailedException("Memeber can't Subscribe to his own Project", OperationType.PROJECT_SUBSCRIPTION);
		}
		throw new ProjectNotFoundException();
	}

	@Override
	public boolean removeSubscribtionToProject(int projectId, int userId) throws ProjectNotFoundException, CollaboratorNotFoundException {
		Project currentProject = findById(projectId);
		if (currentProject != null) {
				Artsuki fetchedArtsuki = isArtsukiExist(userId);
				if (fetchedArtsuki == null ) throw new CollaboratorNotFoundException();
				currentProject.removeSubscriber(fetchedArtsuki);
				em.merge(currentProject);
				return true;
		}
		throw new ProjectNotFoundException();
	}

	@Override
	public Set<Artsuki> getListSbuscribers(Project project) throws ProjectNotFoundException {
		Project currentProject = findById(project.getId());
		if (currentProject != null) {
			return currentProject.getSubscribers();
		}
		throw new ProjectNotFoundException();
	}

	@Override
	public Set<Project> getListOfSubscribe(Artsuki artsuki) throws CollaboratorNotFoundException {
		Artsuki fetchedArtsuki = isArtsukiExist(artsuki.getId());
		return fetchedArtsuki.getSubscribedProjects();
	}

	@Override
	public boolean createDefaultProject(Artsuki artsuki) throws CollaboratorNotFoundException, HasDefaultProjectException {
		Artsuki fetchedArtsuki = isArtsukiExist(artsuki.getId());
		Project defaultProject = new Project();
		
		hasDefaultProject(fetchedArtsuki);
		System.out.println("Is null == "+fetchedArtsuki==null);
		defaultProject.setPublisher(fetchedArtsuki);
		defaultProject.setDescription(fetchedArtsuki.getUsername()+"'s default project");
		defaultProject.setTitle(fetchedArtsuki.getUsername()+"Default");
		defaultProject.setCategory(CategoryProject.PERSONAL);
		addProject(defaultProject);
		return true;
	}
	
	public boolean hasDefaultProject(Artsuki artsuki) throws HasDefaultProjectException {
		Query seearchQuery = em.createQuery("FROM Project p "
				+ "WHERE p.category = 'PERSONAL'"
				+ " AND "
				+ "p.publisher =:publisher");
		seearchQuery.setParameter("publisher", artsuki);
		if( !seearchQuery.getResultList().isEmpty())
			throw new HasDefaultProjectException();
		else 
			return false;
	}

	@Override
	public List<Project> getProjectsPerUser(int userId) throws CollaboratorNotFoundException {
		Artsuki fetchedArtsuki = isArtsukiExist(userId);
		Query seearchQuery = em.createQuery("FROM Project p "
				+ "WHERE p.publisher =:publisher");
		seearchQuery.setParameter("publisher", fetchedArtsuki);
		return seearchQuery.getResultList();
	}
	
	public static boolean contains(String cat) {

	    for (CategoryProject category : CategoryProject.values()) {
	        if (category.name().equals(cat)) {
	            return true;
	        }
	    }

	    return false;
	}

	@Override
	public int getNbrArtworkInProject(Project project) throws ProjectNotFoundException {
		Project fetchedProject = findById(project.getId());
		if( fetchedProject == null ) throw new ProjectNotFoundException();
		Query searchQuery = em.createQuery("SELECT count(a.id) FROM Artwork a WHERE a.project = :project");
		searchQuery.setParameter("project", fetchedProject);
		return (int) searchQuery.getSingleResult();
	}
	
	public double getProjectRate(Project project) throws ProjectNotFoundException {
		Project fetchedProject = findById(project.getId());
		Query searchQuery = em.createQuery("SELECT SUM(r.value)/COUNT(r.publication) "
				+ "FROM Rating r "
				+ "JOIN r.publication p "
				+ "WHERE p.id IN "
				+ "(SELECT a.id FROM Artwork a WHERE project = :project )");
		searchQuery.setParameter("project", project);
		return (double) searchQuery.getSingleResult();
	}

	@Override
	public void seedArtworkAndRate() throws ProjectNotFoundException {
		Artsuki artsuki;
		Artsuki artsuki2;
		try {
			artsuki = isArtsukiExist(2);
			artsuki2 = isArtsukiExist(3);
			Artwork artwork1 = new Artwork();
			Artwork artwork2 = new Artwork();
			Artwork artwork3 = new Artwork();
			Rating rate1 = new Rating();
			Rating rate2 = new Rating();
			Rating rate3 = new Rating();
			Project project = findById(8);
			
			artwork1.setDatePub(new Timestamp(new Date().getTime()));
			artwork1.setDescription("Lorme Ipsum doler kuntim solarus");
			artwork1.setForSale(false);
			artwork1.setMediaType(Media.VISUAL.toString());
			artwork1.setPrice(0);
			artwork1.setPublicationType(PublicationType.Artwork);
			artwork1.setPublisher(artsuki);
			artwork1.setTitle("LaJacondePartik");
			artwork1.setProject(project);

			artwork2.setDatePub(new Timestamp(new Date().getTime()));
			artwork2.setDescription("Lorme Ipsum doler kuntim solarus");
			artwork2.setForSale(false);
			artwork2.setMediaType(Media.VISUAL.toString());
			artwork2.setPrice(0);
			artwork2.setPublicationType(PublicationType.Artwork);
			artwork2.setPublisher(artsuki);
			artwork2.setTitle("LaJacondePartik");
			artwork2.setProject(project);
			
			artwork3.setDatePub(new Timestamp(new Date().getTime()));
			artwork3.setDescription("Lorme Ipsum doler kuntim solarus");
			artwork3.setForSale(false);
			artwork3.setMediaType(Media.VISUAL.toString());
			artwork3.setPrice(0);
			artwork3.setPublicationType(PublicationType.Artwork);
			artwork3.setPublisher(artsuki);
			artwork3.setTitle("LaJacondePartik");
			artwork3.setProject(project);
			
			em.persist(artwork1);
			em.persist(artwork2);			
			em.persist(artwork3);
			
			rate1.setValue(5);
			rate1.setPublication(artwork1);
			rate1.setRater(artsuki2);
			
			rate2.setValue(5);
			rate2.setPublication(artwork2);
			rate2.setRater(artsuki2);
			
			rate3.setValue(5);
			rate3.setPublication(artwork3);
			rate3.setRater(artsuki2);
			
			em.persist(rate1);
			em.persist(rate2);
			em.persist(rate3);
		} catch (CollaboratorNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean increaseNumberVisitor(Project project, String ipAddress) throws ProjectNotFoundException {
		Visitor visitor = new Visitor();
		visitor.setProject(project);
		visitor.setIpAddress(ipAddress);
		em.persist(visitor);
		return false;
	}

	@Override
	public long numberOfView(Project project) throws ProjectNotFoundException {
		Project fetchedProject = findById(project.getId());
		Query searchQuery = em.createQuery("SELECT COUNT(DISTINCT v.ipAddress) FROM Visitor v WHERE v.project = :project");
		searchQuery.setParameter("project", fetchedProject);
		return (long) searchQuery.getSingleResult();
	}
	public long numberOfCollaborators(int id) throws ProjectNotFoundException {
		Project fetchedProject = findById(id);
		if (fetchedProject == null) throw new ProjectNotFoundException() ;
		return fetchedProject.getCollaborators().size();
	}
	public long numberOfSubscriberss(int id) throws ProjectNotFoundException {
		Project fetchedProject = findById(id);
		if (fetchedProject == null) throw new ProjectNotFoundException() ;
		return fetchedProject.getSubscribers().size();
	}
}
