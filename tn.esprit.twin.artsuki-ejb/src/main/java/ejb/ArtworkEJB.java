package ejb;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import entity.Artwork;
import entity.CategoryArtwork;

@Stateless
public class ArtworkEJB implements ArtworkLocal, ArtworkRemote {
	@PersistenceContext(unitName = "tn.esprit.twin.artsuki-ejb")
	EntityManager em;

	@Override
	public boolean addArtwork(Artwork artwork) {
		artwork.setDatePub(new Timestamp(System.currentTimeMillis()));
		artwork.setForSale(true);
		em.persist(artwork);
		return true;
	}

	@Override
	public boolean modifyArtwork(Artwork artwork, int idCurrentUser) throws ArtworkNotFoundException {
		if (artwork != null) {
			Artwork currentArtwork = findById(artwork.getId());
			if (currentArtwork.getPublisher().getId() == idCurrentUser) {
				currentArtwork.setTitle(artwork.getTitle());
				currentArtwork.setDescription(artwork.getDescription());
				currentArtwork.setCategory(artwork.getCategory());
				currentArtwork.setPrice(artwork.getPrice());
				currentArtwork.setMediaPath(artwork.getMediaPath());
				em.merge(currentArtwork);
				return true;
			}
			throw new ArtworkNotFoundException();
		} else
			return false;
	}

	@Override
	public boolean sellArtwork(int id) throws ArtworkNotFoundException {
		if (id != 0) {
			Artwork currentArtwork = findById(id);
			currentArtwork.setForSale(false);
			em.merge(currentArtwork);
			return true;
		} else
			return false;
	}

	@Override
	public boolean deleteArtwork(Artwork artwork, int idCurrentUser) throws ArtworkNotFoundException {
		if (artwork != null) {
			Artwork currentArtwork = findById(artwork.getId());
			if (currentArtwork.getPublisher().getId() == idCurrentUser) {
				em.remove(currentArtwork);
				return true;
			}
			throw new ArtworkNotFoundException();
		} else
			return false;
	}
	
	@Override
	public boolean deleteArtworkById(int id, int idCurrentUser) throws ArtworkNotFoundException {
		if (id != 0) {
			Artwork currentArtwork = findById(id);
			if (currentArtwork.getPublisher().getId() == idCurrentUser) {
				em.remove(currentArtwork);
				return true;
			}
			throw new ArtworkNotFoundException();
		} else
			return false;
	}

	@Override
	public List<Artwork> getAllArtwork() {
		return em.createQuery("from Artwork ORDER BY id DESC").getResultList();
	}

	@Override
	public Artwork findById(int id) {
		return em.find(Artwork.class, id);
	}

	@Override
	public List<Artwork> findByCategory(String category) {
		List<Artwork> artworks = em.createQuery("from Artwork").getResultList();
		List<Artwork> finalArtworks = new ArrayList<Artwork>();
		for (Artwork currentArtwork : artworks) {
			if (category.equals(currentArtwork.getCategory().toString())) {
				finalArtworks.add(currentArtwork);
			}
		}
		return finalArtworks;
	}

	@Override
	public List<Artwork> findByPublisher(int idPublisher) {
		List<Artwork> artworks = em.createQuery("from Artwork").getResultList();
		List<Artwork> finalArtworks = new ArrayList<Artwork>();
		for (Artwork currentArtwork : artworks) {
			if (idPublisher == currentArtwork.getPublisher().getId()) {
				finalArtworks.add(currentArtwork);
			}
		}
		return finalArtworks;
	}

	@Override
	public List<Artwork> filterByPrice(Double minPrice, Double maxPrice) {
		TypedQuery<Artwork> query = em.createQuery(
				"SELECT a FROM Artwork a WHERE a.price >= :minPrice AND a.price <= :maxPrice", Artwork.class);
		query.setParameter("minPrice", minPrice);
		query.setParameter("maxPrice", maxPrice);
		List<Artwork> artworks = query.getResultList();
		return artworks;
	}

	@Override
	public List<Artwork> findByCriteria(CategoryArtwork category, Double price, String mediaType) {
		List<Artwork> artworks = new ArrayList<Artwork>();

		if ((category == null) && (price == null) && (mediaType == null)) {
			getAllArtwork();

		}else if ((category != null) && (price == null) && (mediaType == null)) {
			artworks = em.createQuery("select a from Artwork a where a.category =:category")
					.setParameter("category", category).getResultList();

		}else if ((category == null) && (price != null) && (mediaType == null)) {
			artworks = em.createQuery("select a from Artwork a where a.price <:price").setParameter("price", price)
					.getResultList();

		}else if ((category == null) && (price == null) && (mediaType != null)) {
			artworks = em.createQuery("select a from Artwork a where a.mediaType =:mediaType")
					.setParameter("mediaType", mediaType).getResultList();

		}else if ((category != null) && (price != null) && (mediaType == null)) {
			artworks = em.createQuery("SELECT a FROM Artwork a WHERE a.category LIKE \'%" + category + "%\' AND a.price <:price")
					.setParameter("price", price).getResultList();

		}else if ((category != null) && (price == null) && (mediaType != null)) {
			artworks = em.createQuery("SELECT a FROM Artwork a WHERE a.category LIKE \'%" + category
					+ "%\' AND a.mediaType LIKE \'%" + mediaType + "%\'").getResultList();

		}else if ((category == null) && (price != null) && (mediaType != null)) {
			artworks = em.createQuery("SELECT a FROM Artwork a WHERE a.mediaType LIKE \'%" + mediaType + "%\' AND a.price <:price'")
					.setParameter("price", price).getResultList();

		}else if ((category != null) && (price != null) && (mediaType != null)) {
			artworks = em.createQuery("SELECT a FROM Artwork a WHERE a.category LIKE \'%" + category+ "%\' AND a.price <:price AND a.mediaType LIKE \'%" + mediaType + "%\'")
					.setParameter("price", price).getResultList();
		}

		if (artworks != null)
			return artworks;
		else
			return null;
	}

	@Override
	public String[] fileUpload() {
		File file = new File("C:\\Users\\bayrem\\Documents\\test.jpg");
		String[] tab = new String[2];
		try {
			Process f = Runtime.getRuntime().exec("explorer.exe /select," + file.getAbsolutePath());
			String mediaPath = file.getAbsolutePath();
			String mediaType = file.getName().substring(file.getName().indexOf(".") + 1);
			System.out.println(mediaPath + "\n" + mediaType);
			tab[0] = mediaPath;
			tab[1] = mediaType;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tab;
	}

}
