package ejb;

import java.util.List;

import javax.ejb.Remote;

import entity.Artwork;
import entity.CategoryArtwork;

@Remote
public interface ArtworkRemote {
	public boolean addArtwork(Artwork artwork);

	public boolean modifyArtwork(Artwork artwork, int idCurrentUser) throws ArtworkNotFoundException;
	
	public boolean sellArtwork(int id) throws ArtworkNotFoundException;

	public boolean deleteArtwork(Artwork artwork, int idCurrentUser) throws ArtworkNotFoundException;

	public boolean deleteArtworkById(int id, int idCurrentUser) throws ArtworkNotFoundException;

	public List<Artwork> getAllArtwork();

	public Artwork findById(int id);

	public List<Artwork> findByCategory(String category);

	public List<Artwork> findByPublisher(int idPublisher);

	public List<Artwork> filterByPrice(Double minPrice, Double maxPrice);

	public List<Artwork> findByCriteria(CategoryArtwork category, Double price, String mediaType);

	public String[] fileUpload();

}
