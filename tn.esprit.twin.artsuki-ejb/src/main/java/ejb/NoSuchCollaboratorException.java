package ejb;

public class NoSuchCollaboratorException extends Exception {

	public NoSuchCollaboratorException(int id) {
		super("NO COLLABORATOR WITH SUCH ID "+id);
		// TODO Auto-generated constructor stub
	}
	
}
