package ejb;

public enum OperationType {
	PROJECT_SUBSCRIPTION,
	PROJECT_COLLABORATION,
	PROJECT_CREATION,
	PROJECT_UPDATE,
	PROJECT_DELETE
}
