package ejb;

import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import entity.Rating;

@Stateful
public class RatingEJB implements RatingRemote, RatingLocal {
	@PersistenceContext(unitName = "tn.esprit.twin.artsuki-ejb")
	EntityManager em;

	@Override
	public int addRate(Rating rating) {
		em.persist(rating);
		return rating.getId();
	}

	@Override
	public void modifyRate(Rating rating, int id) {
		Rating currentRating = em.find(Rating.class, id);
		if (currentRating != null) {
			currentRating.setValue(rating.getValue());
			em.merge(currentRating);
		}
	}

	@Override
	public void deleteRate(int id) {
		em.remove(em.find(Rating.class, id));

	}

	@Override
	public Rating findRating(int id) {
		return em.find(Rating.class, id);
	}

	@Override
	public Float max() {
		Query query = em.createQuery("SELECT MAX(r.value) FROM Rating r");
		return (Float) query.getSingleResult();
	}

	@Override
	public int idMax() {
		Query fl = em.createQuery("SELECT MAX(r.value) FROM Rating r");
		Float flo = (Float) fl.getSingleResult();
		Query query = em.createQuery("SELECT r.publication.id FROM Rating r where r.value=:flo");
		query.setParameter("flo", flo);
		return (int) query.getSingleResult();
	}

	// -------------- ARTWORK --------------//
 
	@Override
	public List<Rating> findRatesByArtwork(int idArt) {
		TypedQuery<Rating> query = em.createQuery("FROM Rating WHERE publication_id =:id", Rating.class);
		query.setParameter("id", idArt);
		List<Rating> rates = query.getResultList();
		return rates;
	}
	
	@Override
	public boolean alreadyRated(Rating rating, int idCurrentUser) {
		boolean test = false;
		int idArt = rating.getPublication().getId();
		List<Rating> rates = findRatesByArtwork(idArt);
		for (Rating rate : rates) {
			if (rate.getRater().getId() == idCurrentUser)
				test = true;
		}
		return test;

	}

	@Override
	public boolean addRatingToArtwork(Rating rating, int idCurrentUser) {
		if (alreadyRated(rating, idCurrentUser)) {
			return false;
		} else {
			em.persist(rating);
			return true;
		}
	}

	@Override
	public boolean modifyRating(Rating rating, int idCurrentUser) throws RatingNotFoundException {
		if ((rating != null) && (rating.getRater().getId() == idCurrentUser)) {
			Rating currentRating = em.find(Rating.class, rating.getId());
			if (currentRating != null) {
				currentRating.setValue(rating.getValue());
				em.merge(currentRating);
				return true;
			}
			throw new RatingNotFoundException();
		} else
			return false;
	}

	@Override
	public List<Rating> getAllRates() {
		TypedQuery<Rating> query = em.createQuery("FROM Rating", Rating.class);
		List<Rating> rates = query.getResultList();
		return rates;
	}

	@Override
	public float findByArtwork(int id) {
		float global = 0f;
		TypedQuery<Rating> query = em.createQuery("FROM Rating WHERE publication_id =:id", Rating.class);
		query.setParameter("id", id);
		List<Rating> rates = query.getResultList();
		for (Rating rating : rates) {
			global = global + rating.getValue();
		}
		global = global / rates.size();
		return global;
	}

}