package ejb;

import java.lang.reflect.Array;
import java.util.List;

import javax.ejb.Remote;

import entity.Artsuki;
import entity.Comment;
import entity.Community;
import entity.Project;
import entity.Rating;

@Remote
public interface CommunityRemote {
	public int addCommunity(Community community);
	public boolean addCommunity1(Community community);
	public int addComment(Comment comment);
	public int addRating(Rating rating);
	public void modifyCommunity(Community community, int id);
	public void deleteCommunity(int id);
	public boolean deleteById(Community community);
	public List<Community> getAllCommunity();
	public void findById(int id);
	public Community findCommunity(int id);
	public void affecterCommentACommunity(int comId, int communityId);
	public String getMessageBiTitle(String titre);
	public Community getThePost(String titre);
	public Community theMostLiked();
	public Community theLastLiked();
	public Long nombrePost();
	public Long idUsers();
//	public Artsuki getTheArtuski();
	public Community getPostWithSignaledComment();
	public Community getThePostWithTheme(String theme);
	public List<Community>  getThePostWithUsername(String userName);
}
