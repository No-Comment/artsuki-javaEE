package ejb;

import java.util.List;

import javax.ejb.Remote;

import entity.Comment;

@Remote
public interface CommentRemote {
	public int addComment(Comment comment);
	public void modifyComment(Comment comment, int id);
	public void deleteComment(int id);
	public boolean deleteById(int id);
	public List<Comment> getAllComment();
	public Comment findById(int id);
	public Comment findThatComment(); 
	
	public boolean addCommentToArtwork(Comment comment);
	public boolean modifyCommentArtwork(Comment comment, int idCurrentUser) throws CommentNotFoundException;
	public boolean deleteCommentArtwork(Comment comment, int idCurrentUser) throws CommentNotFoundException;
	public List<Comment> findByArtwork(int id);
	public Comment findCommentById(int id);

}
