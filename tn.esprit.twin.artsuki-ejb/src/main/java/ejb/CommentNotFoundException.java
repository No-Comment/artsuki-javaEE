package ejb;

public class CommentNotFoundException extends Exception{

	public CommentNotFoundException() {
		super("Comment NOT FOUND! It seems that you are not the owner of this comment");
	}
	
}
