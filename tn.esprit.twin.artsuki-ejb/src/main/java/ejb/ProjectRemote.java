package ejb;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;

import entity.Artsuki;
import entity.Project;

@Remote
public interface ProjectRemote {
	public boolean addProject(Project project);
	public boolean modifyProject(Project project) throws ProjectNotFoundException;
	public boolean deleteProject(Project project) throws ProjectNotFoundException;
	public boolean addCollaborator(Project project, int artsukiId) throws ProjectNotFoundException, CollaboratorNotFoundException;
	public boolean removeCollaborator(Project project, int artsukiId) throws ProjectNotFoundException, CollaboratorNotFoundException, NoSuchCollaboratorException;
	public List<Project> getAllProject();
	public Project findById(int id) throws ProjectNotFoundException;
	public List<Project> findByCategory(String category) throws NoSuchCategoryException;
	public List<Project> findByTitle(String title);
	public void addTestUserProject();
	public void seedArtworkAndRate() throws ProjectNotFoundException;
	public Artsuki isArtsukiExist(int id) throws CollaboratorNotFoundException;
	public List<Project> getProjectsByKeyword(String keyword);
	public boolean subscribeToProject(int projectId, int userId) throws ProjectNotFoundException, CollaboratorNotFoundException, OperationFailedException;
	public boolean removeSubscribtionToProject(int projectId, int userId) throws ProjectNotFoundException, CollaboratorNotFoundException; 
	public Set<Artsuki> getListSbuscribers(Project project) throws ProjectNotFoundException;
	public Set<Project> getListOfSubscribe(Artsuki artsuki) throws CollaboratorNotFoundException;
	public boolean createDefaultProject(Artsuki artsuki) throws CollaboratorNotFoundException, HasDefaultProjectException;
	public List<Project> getProjectsPerUser(int userId) throws CollaboratorNotFoundException;
	public int getNbrArtworkInProject(Project project) throws ProjectNotFoundException;
	public double getProjectRate(Project project) throws ProjectNotFoundException;
	public boolean increaseNumberVisitor(Project project, String ipAddress)throws ProjectNotFoundException;
	public long numberOfView(Project project) throws ProjectNotFoundException;
	public List<Project> getProjects(int page);
	public long getProjectNumber();
	public long numberOfCollaborators(int id) throws ProjectNotFoundException;
	public long numberOfSubscriberss(int id) throws ProjectNotFoundException;
}
