package ejb;

import java.util.List;

import javax.ejb.Local;

import entity.Community;
import entity.Rating;

@Local
public interface RatingLocal {
	public int addRate(Rating rating);
	public void modifyRate(Rating rating, int id);
	public void deleteRate(int id);
	public Rating findRating(int id);
	public Float max();
	public int idMax();

	public List<Rating> findRatesByArtwork(int idArt);
	public boolean alreadyRated(Rating rating, int idCurrentUser);
	public boolean addRatingToArtwork(Rating rating, int idCurrentUser);
	public boolean modifyRating(Rating rating, int idCurrentUser) throws RatingNotFoundException;
	public float findByArtwork(int id);
	public List<Rating> getAllRates();

}
