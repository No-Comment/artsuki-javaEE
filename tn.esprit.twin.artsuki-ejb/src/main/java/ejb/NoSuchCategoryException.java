package ejb;

public class NoSuchCategoryException extends Exception{
	public NoSuchCategoryException() {
		super("NO SUCH CATEGORY IN OUR DATABASE");
	}
}
