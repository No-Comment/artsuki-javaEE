package ejb.artsuki;

public class ArtsukiNotFoundException extends Exception {

	public ArtsukiNotFoundException() {
	}

	public ArtsukiNotFoundException(String message) {
		super(message);
	}

	public ArtsukiNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ArtsukiNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ArtsukiNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
