package ejb.artsuki;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotFoundException;

import entity.Artsuki;
import entity.Friendship;
import entity.FriendshipStatus;

@Stateless
public class FriendshipEJB implements FriendshipLocal,FriendshipRemote{

	@PersistenceContext(unitName="tn.esprit.twin.artsuki-ejb")
	EntityManager em;
	
	@EJB
	private ArtsukiRemote artsukiManager;
	
	@Override
	public List<Artsuki> getAllInvitations(Artsuki artsuki) {
		List<Artsuki> invitationsDB=em.createQuery("SELECT fs.Requester.id,fs.Requester.firstname,fs.Requester.lastname "
				+ "FROM Friendship fs "
				+ "WHERE Receiver.id= :r AND status = 'PENDING' ")
				.setParameter("r", artsuki.getId())
				.getResultList()
				;
		
		return invitationsDB;
	}

	@Override
	public List<Artsuki> getAllFriends(Artsuki artsuki) {
		List<Artsuki> invitationsDB=em.createQuery("SELECT fs.Requester.id,fs.Requester.firstname,fs.Requester.lastname "
				+ "FROM Friendship fs "
				+ "WHERE Receiver.id= :r AND status = 'FRIENDS' ")
				.setParameter("r", artsuki.getId())
				.getResultList()
				;
		
		return invitationsDB;
	}

	@Override
	public void sendInvitation(Artsuki onlineArtsuki,Artsuki artsuki) {
		Artsuki from=artsukiManager.findArtsukiById(onlineArtsuki.getId());
		Artsuki to=artsukiManager.findArtsukiById(artsuki.getId());
		Friendship f=new Friendship();
		f.setStatus(FriendshipStatus.PENDING);
		em.persist(f);
		f.setRequester(from);
		f.setReceiver(to);
		
	}

	@Override
	public void respondToInvitation(Artsuki OnlineArtsuki, Friendship friendship) throws NotAcceptableException,NotFoundException,NullPointerException 
	{
		
		Friendship fs=em.find(Friendship.class, friendship.getId());
		Artsuki online=em.find(Artsuki.class, OnlineArtsuki.getId());
		if(fs!=null)
		{
			if(fs.getReceiver()==online)
			{
				fs.setStatus(FriendshipStatus.FRIENDS);
			}
			else
			{
				throw new NotAcceptableException();
			}
		}
		else
		{
			throw new NotFoundException();
		}
	}

	@Override
	public void deleteFriend(Artsuki OnlineArtsuki, Friendship friendship) throws NotAcceptableException,NotFoundException,NullPointerException
	{
		Friendship fs=em.find(Friendship.class, friendship.getId());
		
		if(fs != null)
		{
			if(fs.getReceiver().getId()==OnlineArtsuki.getId() || fs.getRequester().getId()==OnlineArtsuki.getId())
			{
				em.remove(fs);
			}
			else{
			throw new NotAcceptableException();
			}
		}
		else {
			throw new NotFoundException();
		}
	}

	@Override
	public List<Friendship> getAllFriendships(Artsuki artsuki) {

		List<Friendship> invitationsDB=em.createQuery("SELECT fs "
				+ "FROM Friendship fs "
				+ "WHERE Receiver.id= :r OR Requester.id= :r ")
				.setParameter("r", artsuki.getId())
				.getResultList()
				;
		
		return invitationsDB;
	}

}
