package ejb.artsuki;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import entity.Artsuki;
import entity.Role;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Stateless
public class ArtsukiEJB implements ArtsukiLocal, ArtsukiRemote {
	

	@PersistenceContext(unitName="tn.esprit.twin.artsuki-ejb")
	EntityManager em;

	
	
	@Override
	public boolean login(String login, String pwd) {
		Query query=em.createQuery("SELECT a FROM Artsuki a WHERE email = :mail AND password = :pwd").setParameter("mail", login).setParameter("pwd", pwd);
		if(query.getResultList().size() == 0)
			return false;
		return true;
	}

	@Override
	public int registerArtsuki(Artsuki artsuki) {
		artsuki.setVerified(false);
		artsuki.setRole(Role.ARTSUKI);
		em.persist(artsuki);
		String confirmationToken=generateConfirmationToken(artsuki,"confirmation888x");
		sendMail(artsuki.getEmail(), "This is the confirmation token: "+confirmationToken+"\n This is confirmation link :http://localhost:57533/Artsuki/confirm?token="+confirmationToken);
		return artsuki.getId();
	}

	private String generateConfirmationToken(Artsuki artsuki,String secret) {
		String keyString = secret;
		Key key = new SecretKeySpec(keyString.getBytes(), 0, keyString.getBytes().length, "DES");
		System.out.println("the key is : " + key);

		String jwtToken = Jwts.builder().setSubject(artsuki.getEmail())
				.setIssuer("Server")
				.setIssuedAt(new Date()).setExpiration(toDate(LocalDateTime.now().plusMinutes(15L)))
				.signWith(SignatureAlgorithm.HS512, key).compact();

		System.out.println("the returned token is : " + jwtToken);
		return jwtToken;		
	}
	
	private Date toDate(LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	@Override
	public Artsuki findArtsukiById(int id) {
		Artsuki artsuki=em.find(Artsuki.class, id);
		return artsuki;
	}

	@Override
	public List<Artsuki> findArtsukiByFirstname(String firstname) {
		
		Query query=em.createQuery("select a from Artsuki a where a.firstname = :firstname");
		query.setParameter("firstname", firstname);
		return query.getResultList();
	}

	@Override
	public List<Artsuki> findArtsukiByLastname(String lastname) {
		List<Artsuki> artsukiList=em.createQuery("select a from Artsuki a where a.Lastname =:lastname").setParameter("lastname", lastname).getResultList();
		return artsukiList;
	}

	@Override
	public boolean deleteArtsuki(Artsuki artsuki) {
		Artsuki as=em.find(Artsuki.class, artsuki.getId());
		em.remove(as);
		return false;
	}

	
	@Override
	public void updateArtsuki(Artsuki updatedArtsuki) {
		
		Artsuki oldArtsuki=em.find(Artsuki.class, updatedArtsuki.getId());
		if(updatedArtsuki.getFirstname() != null)
			oldArtsuki.setFirstname(
					updatedArtsuki.getFirstname());
		
		if(updatedArtsuki.getLastname() != null )
			oldArtsuki.setLastname(updatedArtsuki.getLastname());

		if(updatedArtsuki.getNationality() != null )
			oldArtsuki.setNationality(updatedArtsuki.getNationality());

		if(updatedArtsuki.getPhoneNumber() != null )
			oldArtsuki.setPhoneNumber(updatedArtsuki.getPhoneNumber());

		if(updatedArtsuki.getGender() != null )
			oldArtsuki.setGender(updatedArtsuki.getGender());

		if(updatedArtsuki.getEmail() != null )
			oldArtsuki.setEmail(updatedArtsuki.getEmail());
		
		if(updatedArtsuki.getCountry() != null )
			oldArtsuki.setCountry(updatedArtsuki.getCountry());
		
		if(updatedArtsuki.getCity() != null )
			oldArtsuki.setCity(updatedArtsuki.getCity());

		if(updatedArtsuki.getStreet() != null )
			oldArtsuki.setStreet(updatedArtsuki.getStreet());

		if(updatedArtsuki.getTown() != null )
			oldArtsuki.setTown(updatedArtsuki.getTown());

		if(updatedArtsuki.getBiography() != null )
			oldArtsuki.setBiography(updatedArtsuki.getBiography());
		
		
		
	}

	@Override
	public String upgradeToArtist(Artsuki artsuki) {
		Artsuki artsukiDB=em.find(Artsuki.class, artsuki.getId());
		if(artsukiDB != null)
		{
			if(artsukiDB.getRole()==Role.ARTIST)
				return "Already an artist";
			artsukiDB.setRole(Role.ARTIST);
			return "Succesfully upgraded";
		}
		return "Artsuki not found";
	}

	@Override
	public Artsuki findArtsukiByEmail(String mail) {
		Query query=em.createQuery("select a from Artsuki a where a.email = :mail").setParameter("mail", mail);
		List<Artsuki> artsukis=query.getResultList();
		if(artsukis.size()>0)
			return artsukis.get(0);
		
			return null;
	}

	@Override
	public String isValidRegistration(Artsuki artsuki) {
		String errors="";
		if(findArtsukiByEmail(artsuki.getEmail()) != null)
		{
			return "An account is already associated with this mail";
		}
		if(artsuki.getFirstname() == null || artsuki.getFirstname().matches(".*\\d+.*") || artsuki.getFirstname().length()<=1)
		{
			errors+="Firstname must be a string of characters\n";
		}
		if(artsuki.getLastname() == null || artsuki.getLastname().matches(".*\\d+.*") || artsuki.getLastname().length()<=1)
		{
			errors+="Lastname must be a string of characters\n";
		}
		if (artsuki.getEmail() == null ||!artsuki.getEmail().matches("^[A-Za-z0-9-\\+]+(\\.[A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,3})$"))
		{
			errors+="Email is not valid\n";
		}
		if (artsuki.getGender() == null || !artsuki.getGender().equals("male") && !artsuki.getGender().equals("Female"))
		{ 
			errors+="Gender is invalid\n";
		}
		if (artsuki.getPassword() == null || artsuki.getPassword().length()<8)
		{ 
			errors+="Your password must contain more than 8 characters\n";
		}
		return errors;
	}

	@Override
	public List<Artsuki> findArtsuki(String data) //This method will search for an artsuki with firstname or lastname
	{
		Query query=em.createQuery("SELECT a.id,a.firstname as firstname ,a.lastname as lastname, a.gender as gender,a.email as email FROM Artsuki a WHERE a.firstname LIKE \'%"+data+"%\' OR a.lastname LIKE \'%"+data+"%\'");
		return query.getResultList();
	}

	public boolean sendMail(String to,String body)

	{		
	Properties props = new Properties();
	props.put("mail.smtp.host", "smtp.gmail.com");
	props.put("mail.smtp.starttls.enable","true");
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.port", "587");

	Session session = Session.getInstance(props,
		new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("artsuki.com@gmail.com","artsukiartsuki");
			}
		});

	try {

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress("artsuki.com@gmail.com"));
		message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
		message.setSubject("Recovery");
		message.setText(body);
		System.out.println("Sending mail...");
		Transport.send(message);

		System.out.println("Mail sent");
		return true;
	} catch (MessagingException e) {
		e.printStackTrace();
		return false;
	}

	
	}
	
	@Override
	public void sendRecoveryMail(String mail) throws MailNotFoundException
	{
		Artsuki artsuki=findArtsukiByEmail(mail);
		if(artsuki != null)
		{
			String token=generateConfirmationToken(artsuki,"recovery123www");
			sendMail(artsuki.getEmail(), "Your recovery token is: "+token);
		}
		else
		{
			throw new MailNotFoundException();
		}
	}
	
	public void updatePassword(String token,Artsuki artsuki)
	{
		String secretkey="recovery123www";
		Key key = new SecretKeySpec(secretkey.getBytes(), 0, secretkey.getBytes().length, "DES");
        String mail=Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getSubject();
        Artsuki artsukiDB=findArtsukiByEmail(mail);
        artsukiDB.setPassword(artsuki.getPassword());
       
	} 
	
	public void confirmAccount(String token,String secretkey) 
	{

			Key key = new SecretKeySpec(secretkey.getBytes(), 0, secretkey.getBytes().length, "DES");
	        String mail=Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getSubject();
	        Artsuki artsuki=findArtsukiByEmail(mail);
	        artsuki.setVerified(true);
	}
	
	
	

}
