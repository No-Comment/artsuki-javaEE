package ejb.artsuki;

public class MailNotFoundException extends Exception {

	public MailNotFoundException() {
		// TODO Auto-generated constructor stub
	}

	public MailNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MailNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MailNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MailNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
