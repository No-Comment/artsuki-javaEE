package ejb.artsuki;

import java.util.List;

import javax.ejb.Remote;
import javax.ws.rs.NotAcceptableException;

import entity.Artsuki;

@Remote
public interface ArtsukiRemote {

	public int registerArtsuki(Artsuki artsuki);
	public String isValidRegistration(Artsuki artsuki);
	public Artsuki findArtsukiById(int id);
	public List<Artsuki> findArtsukiByFirstname(String lastname);
	public List<Artsuki> findArtsukiByLastname(String lastname);
	public Artsuki findArtsukiByEmail(String mail);
	public boolean deleteArtsuki(Artsuki artsuki);
	public List<Artsuki> findArtsuki(String data);
	public void updateArtsuki(Artsuki updatedArtsuki);
	public String upgradeToArtist(Artsuki artsuki);
	public void updatePassword(String token,Artsuki artsuki);
	public boolean login(String login,String pwd);
	public void sendRecoveryMail(String mail) throws MailNotFoundException;
	public void confirmAccount(String token,String secretkey) throws NotAcceptableException;

}
