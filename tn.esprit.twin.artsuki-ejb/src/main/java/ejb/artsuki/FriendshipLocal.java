package ejb.artsuki;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotFoundException;

import entity.Artsuki;
import entity.Friendship;

@Local
public interface FriendshipLocal {

	public List<Artsuki> getAllInvitations(Artsuki artsuki);
	public List<Artsuki> getAllFriends(Artsuki artsuki);
	public void sendInvitation(Artsuki onlineArtsuki,Artsuki artsuki);
	public void respondToInvitation(Artsuki OnlineArtsuki,Friendship friendship) throws NotAcceptableException,NotFoundException,NullPointerException;
	public void deleteFriend(Artsuki OnlineArtsuki,Friendship friendship) throws NotAcceptableException,NotFoundException,NullPointerException;
	
}
