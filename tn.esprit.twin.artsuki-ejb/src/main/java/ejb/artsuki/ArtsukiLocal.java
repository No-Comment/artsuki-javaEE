package ejb.artsuki;

import java.util.List;

import javax.ejb.Local;
import javax.ws.rs.NotAcceptableException;

import entity.Artsuki;

@Local
public interface ArtsukiLocal {

	public int registerArtsuki(Artsuki artsuki);
	public Artsuki findArtsukiById(int id);
	public List<Artsuki> findArtsukiByFirstname(String lastname);
	public List<Artsuki> findArtsukiByLastname(String lastname);
	public List<Artsuki> findArtsuki(String data);
	public boolean deleteArtsuki(Artsuki artsuki);
	public void updateArtsuki(Artsuki updatedArtsuki);
	public String upgradeToArtist(Artsuki artsuki);
	public void sendRecoveryMail(String mail) throws MailNotFoundException;
	public void confirmAccount(String token,String secretkey) throws NotAcceptableException;
	public void updatePassword(String token,Artsuki artsuki);

}
