package ejb;

public class ProjectNotFoundException extends Exception{

	public ProjectNotFoundException() {
		super("PROJECT NOT FOUND!");
	}
	
}
