package ejb;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import entity.Artsuki;
import entity.Comment;
import entity.Community;
import entity.Project;
import entity.Rating;

@Stateful
public class CommunityEJB implements CommunityRemote,CommunityLocal{
	@PersistenceContext(unitName="tn.esprit.twin.artsuki-ejb")
	EntityManager em;
	List usersID;
	List longs;

	@Override
	public int addCommunity(Community community) {
		em.persist(community);
		return community.getId();
	}

	@Override
	public void modifyCommunity(Community community, int id) {
		Community currentCommunity = em.find(Community.class, id);
		if (currentCommunity != null) {
			currentCommunity.setAttachement(community.getAttachement());
			currentCommunity.setMessage(community.getMessage());
			currentCommunity.setTheme(community.getTheme());
			em.merge(currentCommunity);
		}
	}

	@Override
	public void deleteCommunity(int id) {
		em.remove(em.find(Community.class, id));
	}

	@Override
	public List<Community> getAllCommunity() {
		return em.createQuery("SELECT o from Community o").getResultList();
	}

	@Override
	public void findById(int id) {
		//return em.find(Community.class, id);
		Query query = em.createQuery("SELECT o FROM Community o");
		List lst = query.getResultList();
		Iterator it = lst.iterator();
		while (it.hasNext()){
			Community community = (Community) it.next();
			System.out.print("Community Title: "+community.getTitle());
			System.out.print("Community Date: "+community.getDatePub());
			System.out.println("Community Message: "+community.getMessage());
			System.out.println("Community Theme: "+community.getTheme());
		}
	}

	@Override
	public boolean deleteById(Community community) {
		Community currentProject = em.find(Community.class, community.getId());
		if (currentProject != null) {
			em.remove(currentProject);
			return true;
		}
		return false;
	}

	@Override
	public int addComment(Comment comment) {
		em.persist(comment);
		return comment.getId();
	}

	@Override
	public int addRating(Rating rating) {
		em.persist(rating);
		return rating.getId();
	}

	@Override
	public boolean addCommunity1(Community community) {
		em.persist(community);
		return true;
	}

	@Override
	public Community findCommunity(int id) {
//		TypedQuery<Community> query = em.createQuery("select c from Community c where c.id=:id",Community.class);
//		query.setParameter("id", id);
//		return query.getSingleResult();
		return null;
	}

	@Override
	public void affecterCommentACommunity(int comId, int communityId) {
		Community communityMangerEntity = em.find(Community.class, communityId);
		Comment commentMangerEntity = em.find(Comment.class, comId);
		commentMangerEntity.setPublication(communityMangerEntity);	
	}
	
	@Override
	public String getMessageBiTitle(String titre) {
		TypedQuery<String> query = em.createQuery("select c.message from Community c where c.title=:titre",String.class);
		query.setParameter("titre", titre);
		return query.getSingleResult();
	}

	@Override
	public Community getThePost(String titre) {
		TypedQuery<Community> query = em.createQuery("select c from Community c where c.title=:titre ",Community.class);
		query.setParameter("titre", titre);
		return query.getSingleResult();
	}

	@Override
	public Community theMostLiked() {
		Query fl = em.createQuery("SELECT MAX(r.value) FROM Rating r");
		Float flo = (Float) fl.getSingleResult();
		Query query = em.createQuery("SELECT r.publication.id FROM Rating r where r.value=:flo" );
		query.setParameter("flo", flo);
		int idP =(int) query.getSingleResult();
		Query query1 = em.createQuery("SELECT c FROM Community c Where c.id=:idP");
		query1.setParameter("idP", idP);
		return (Community) query1.getSingleResult();
	}

	@Override
	public Community theLastLiked() {
		Query fl = em.createQuery("SELECT MIN(r.value) FROM Rating r");
		Float flo = (Float) fl.getSingleResult();
		Query query = em.createQuery("SELECT r.publication.id FROM Rating r where r.value=:flo" );
		query.setParameter("flo", flo);
		int idP =(int) query.getSingleResult();
		Query query1 = em.createQuery("SELECT c FROM Community c Where c.id=:idP");
		query1.setParameter("idP", idP);
		return (Community) query1.getSingleResult();
	}

	@Override
	public Long nombrePost() {
		Query query = em.createQuery("select count(u) from Community u where u.publisher.id=1");
		return (Long) query.getSingleResult();
	}

	@Override
	public Long idUsers() {
		Query query = em.createQuery("select u.id from Artsuki u");
		usersID =query.getResultList();
		Iterator it = usersID.iterator();
		while (it.hasNext()){
			Query query1 = em.createQuery("select count(u) from Community u where u.publisher.id=:it");
			query1.setParameter("it", it);
			return (Long) query1.getSingleResult();
		}
		return null;
	}

	@Override
	public Community getPostWithSignaledComment() {
		Query query = em.createQuery("SELECT MAX(c.nbrSignla) FROM Comment c");
		int flo = (int) query.getSingleResult();
		Query query2 = em.createQuery("SELECT r.publication.id FROM Comment r where r.nbrSignla=:flo" );
		query2.setParameter("flo", flo);
		int idP =(int) query2.getSingleResult();
		Query query1 = em.createQuery("SELECT c FROM Community c Where c.id=:idP");
		query1.setParameter("idP", idP);
		return (Community) query1.getSingleResult();
	}

	@Override
	public Community getThePostWithTheme(String theme) {
		TypedQuery<Community> query = em.createQuery("SELECT c from Community c where c.theme=:theme",Community.class);
		query.setParameter("theme", theme);
		return query.getSingleResult();
	}

	@Override
	public List<Community> getThePostWithUsername(String userName) {
		Query query = em.createQuery("SELECT u.id FROM Artsuki u where u.username=:userName");
		query.setParameter("userName", userName);
		int idUser = (int) query.getSingleResult();
		Query query1 = em.createQuery("SELECT c from Community c WHERE c.publisher.id=:idUser");
		query1.setParameter("idUser", idUser);
		return (List<Community>) query1.getResultList();
	}

//	@Override
//	public Artsuki getTheArtuski() {
//		Query query = em.createQuery("select u.id from Artsuki u");
//		usersID =query.getResultList();
//		Iterator it = usersID.iterator();
//		while (it.hasNext()){
//			Query query1 = em.createQuery("select count(u) from Community u where u.publisher.id=:it");
//			query1.setParameter("it", it);
//			longs=query1.getResultList();
//			Iterator it2 = longs.iterator();
//			Long max = (Long) Collections.max(longs);
//			while(it2.hasNext()){
//				Query query2 = em.createQuery("select u from Artsuki u where u.id=:it2");
//				query2.setParameter("it2", it2);
//				return (Artsuki) query2.getSingleResult();
//			}
//		}
//		return null;
//	}
	
}