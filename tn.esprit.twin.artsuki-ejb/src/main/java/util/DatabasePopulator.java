package util;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ejb.ProjectNotFoundException;
import ejb.ProjectRemote;

@Singleton
@Startup
public class DatabasePopulator {
	@EJB
	private ProjectRemote projectManager;
	@PersistenceContext(unitName="tn.esprit.twin.artsuki-ejb")
	EntityManager em;
	
	
	@PostConstruct
	public void seedDatabase() throws ProjectNotFoundException {
		//projectManager.addTestUserProject();
		//projectManager.seedArtworkAndRate();
	}


}
